
<?php $__env->startSection('title','Pais Crear'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Dotación</h5>
            </div>

            <hr class="mb-4">
            <div class="ibox-content col-lg-6 offset-sm-3 mt-4">
                <?php echo Form::open(['route'=> 'dotacion.store', 'method'=>'POST', 'files' => true ]); ?>

                <?php echo Form::hidden('proyecto_id', $id); ?>

                <div class="form-group">
                    <H1>DOTACIÓN</H1>
                    <div class="row">

                        <label class="col-sm-2">Archivo </label>
                        <div id="contenedorFiles" title="Archivos admitidos: pdf, xlsx, pptx, docx, rar - Tamaño máximo 100 Megas" class=" form-row col-sm-8">
                            <input type='file' id="archivo-1" name='dotacion_file'  class="form-control col-sm-11 contenedorNro mb-2" h-cont="1">

                        </div>

                    </div>

                </div>
                <div class="text-center">
                    <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ausentismo</h5>
            </div>

            <hr class="mb-4">
            <div class="ibox-content col-lg-6 offset-sm-3 mt-4">
                <?php echo Form::open(['route'=> 'ausentismo.store', 'method'=>'POST', 'files' => true ]); ?>

                <?php echo Form::hidden('proyecto_id', $id); ?>

                <div class="form-group">
                    <H1>Ausentismo</H1>
                    <div class="row">

                        <label class="col-sm-2">Archivo </label>
                        <div id="contenedorFiles" title="Archivos admitidos: pdf, xlsx, pptx, docx, rar - Tamaño máximo 100 Megas" class=" form-row col-sm-8">
                            <input type='file' id="archivo-2" name='ausentismo_file'  class="form-control col-sm-11 contenedorNro mb-2" h-cont="1">

                        </div>

                    </div>

                </div>
                <div class="text-center">
                    <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>

    </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>