@extends('layouts.app2')
@section('title','Cuestionario index')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de Usuarios</h4>
            </div>
            <hr class="mb-4">


            <div class="table-responsive">
                <table class="table table-hover" id="dataTableAusentismo" width="100%" cellspacing="0">
                    <thead>
	                    <tr>
                            <th>Evaluado</th>
                            <th>Cargo</th>
	                        <th>Pregunta 1</th>
                            <th>Pregunta 2</th>
                            <th>Pregunta 3</th>
                            <th>Pregunta 4</th>
                            <th>Pregunta 5</th>
                            <th>Pregunta 6</th>
                            <th>Evaluador Asignado</th>
	                    </tr>
                    </thead>
                    <tbody>

                        

                    @foreach($reporte as $us)
                        
                        <tr>
                            <td><small>{{ $us->name}}</small></td>
                            <td><small>{{ $us->usu_cargo}}</small></td>
                            <td><small>{{ $us->respuesta1}}</small></td>
                            <td><small>{{ $us->respuesta2}}</small></td>
                            <td><small>{{ $us->respuesta3}}</small></td>
                            <td><small>{{ $us->respuesta4}}</small></td>
                            <td><small>{{ $us->respuesta5}}</small></td>
                            <td><small>{{ $us->respuesta6}}</small></td>
                            <td><small>{{ $us->jefatura_name}}</small></td>

                           

                        </tr>
                        
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            </div>
        </div>
    </div>
@stop