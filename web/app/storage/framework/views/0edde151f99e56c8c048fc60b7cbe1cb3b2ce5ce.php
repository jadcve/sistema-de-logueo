
<?php $__env->startSection('title','Empresa index'); ?>
<?php $__env->startSection('content'); ?>

    <section class="content">

        <table class ='table table-bordered'>
            <thead class="ibox-title">
                <th><h5>Documento</h5></th>
            </thead>
        </table>
        <table class ='table table-bordered'>
            
            <tbody>
            <?php if($proyectos[0]->url_path == NULL and $proyectos[0]->url_web != NULL ): ?>

                <div>

                    <div  style="position: relative;" oncontextmenu="return false">

                        <div style="position: absolute; top: 865px; right: 0px;">

                            <img border="0" height="35" src=<?php echo e(asset('img/capa_iframe.png')); ?> width="190" />

                        </div>

                        <iframe src= <?php echo e($proyectos[0]->url_web); ?>  width="100%" height="900"></iframe>

                    </div>

                </div>

                <?php elseif($proyectos[0]->url_path != NULL and $proyectos[0]->url_web == NULL and $proyectos[0]->url_tipo == "pdf"  ): ?>

            <tr>
                <td>
                    <div  style="position: relative;" oncontextmenu="return false">

                        <embed src="<?php echo e($path = Storage::disk('s3')->temporaryUrl($proyectos[0]->url_path, now()->addMinutes(1))); ?>" style="width:100%; height:900px;" frameborder="0">
                    </div>
                </td>


            </tr>
            <?php elseif($proyectos[0]->url_path != NULL and $proyectos[0]->url_web == NULL and $proyectos[0]->url_tipo != "pdf" ): ?>
                <td>
                    <h5 class="text-center m-4">Su archivo se encuentra en proceso de descarga</h5>
                     <p class="text-center m-4"><i class="fas fa-cloud-download-alt"></i></p>
                    <div  style="position: relative;" oncontextmenu="return false">


                        <embed src="<?php echo e($path = Storage::disk('s3')->temporaryUrl($proyectos[0]->url_path, now()->addMinutes(1))); ?>" style="width:100%; height:100px;" frameborder="0">

                    </div>
                </td>

            <?php else: ?>
                 <td>NO SE ENCUENTRAN DOCUMENTOS CARGADOS EN ESTE MOMENTO </td>
                    <p class="text-center m-4 icono-nube">
                        <i class="fas fa-cloud-download-alt" aria-hidden="true"></i>
                    </p>
            <?php endif; ?>

            </tbody>
        </table>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>