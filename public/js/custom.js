$( document ).ready(function() {

    //funcion para desactivar una proyectos
    $('.desactivarProyectos').change(function() 
    {
        elemento = this;

        $(elemento).prop("disabled", true);
        var proyecto_id = $(elemento).attr('h');
        var proyecto_estado = $(elemento).val();
        
        $.ajax({
          url:BaseURL+'/desactivarProyectos',
          data: {proyecto_id:proyecto_id, proyecto_estado:proyecto_estado},
          method:'post',
          dataType: 'json',
          headers: {
                    'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
                  },
          success:function(response){

            if (proyecto_estado == 1) 
            {
                var nuevo_estado = 0;
            }else{
                var nuevo_estado = 1;
            }

            if (response.status == true) 
            {
                $('[h="'+proyecto_id+'"]').val(nuevo_estado);
                
                alert('El estatus inactivo/activo ha sido cambiado.');
            }else{
                $('[h="'+proyecto_id+'"]').prop( "checked", false );
                alert('No se ha podido cambiar el status.');
            }
            
            $(elemento).prop("disabled", false);
            
          }
        })
    });

  	//funcion para desactivar una empresa
    $('.desactivar').change(function() 
    {
    	elemento = this;

    	$(elemento).prop("disabled", true);
    	var empresa_id = $(elemento).attr('h');
    	var empresa_estado = $(elemento).val();
      	
	    $.ajax({
	      url:BaseURL+'/desactivarEmpresa',
	      data: {empresa_id:empresa_id, empresa_estado:empresa_estado},
	      method:'post',
	      dataType: 'json',
	      headers: {
	                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
	              },
	      success:function(response){

	      	if (empresa_estado == 1) 
	      	{
	      		var nuevo_estado = 0;
	      	}else{
	      		var nuevo_estado = 1;
	      	}

	      	if (response.status == true) 
	      	{
	      		$('[h="'+empresa_id+'"]').val(nuevo_estado);
	      		
	      		alert('El estatus inactivo/activo ha sido cambiado.');
	      	}else{
	      		$('[h="'+empresa_id+'"]').prop( "checked", false );
	      		alert('No se ha podido cambiar el status.');
	      	}
	      	
	      	$(elemento).prop("disabled", false);
	        
	      }
    	})
    });

    //funcion para desactivar un usuario
    $('.desactivarUsuario').change(function() 
    {
    	elemento = this;

    	$(elemento).prop("disabled", true);
    	var usuario_id = $(elemento).attr('h');
    	var usuario_estado = $(elemento).val();
      	
	    $.ajax({
	      url:BaseURL+'/desactivarUsuario',
	      data: {usuario_id:usuario_id, usuario_estado:usuario_estado},
	      method:'post',
	      dataType: 'json',
	      headers: {
	                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
	              },
	      success:function(response){

	      	if (usuario_estado == 1) 
	      	{
	      		var nuevo_estado = 0;
	      	}else{
	      		var nuevo_estado = 1;
	      	}

	      	if (response.status == true) 
	      	{
	      		$('[h="'+usuario_id+'"]').val(nuevo_estado);
	      		
	      		alert('El status ha sido cambiado.');
	      	}else{
	      		$('[h="'+usuario_id+'"]').prop( "checked", false );
	      		alert('No se ha podido cambiar el status.');
	      	}
	      	
	      	$(elemento).prop("disabled", false);
	        
	      }
    	})
    });
    
});


function cambiarSubrubro(esto)
{
    //**** busca los subrubros dependiendo del rubro seleccinoado

    var rubro_id = $(esto).val();
    
    $.ajax({
      url:BaseURL+'/obtenerSubRubros',
      data: {rubro_id:rubro_id},
      method:'post',
      dataType: 'json',
      headers: {
                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
              },
      success:function(response){
        
        $('#sub_id').empty();
        
        $.each(response.subrubros, function( index, value ) { 
            
          $('#sub_id').append("<option value='"+index+"'>"+value+"</option>"
              );
         });

      }
    });
    
}

function cambiarResponsable(esto)
{
	
	var empresa_id = $("#emp_id option:selected").val();

    $.ajax({
      url:BaseURL+'/obtenerResponsables',
      data: {empresa_id:empresa_id},
      method:'post',
      dataType: 'json',
      headers: {
                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
              },
      success:function(response){
        
        $('#res_id').empty();
        
        $.each(response.responsables, function( index, value ) { 
            
          $('#res_id').append("<option value='"+index+"'>"+value+"</option>"
              );
         });

      }
    });
	
} 


//Carga DATA
$(document).ready(function () {

    var navListItems = $('div.setup-panel div button'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
        $item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div button[href="#' + curStepBtn + '"]').parent().next().children("button"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
  

    $('div.setup-panel div button.btn-primary').trigger('click');
});
//file




//Carga de data acordeon

function cambiarIcono(texto)
{
    $('#'+ texto).toggleClass('fa-plus fa-minus');
}

function mostrarData(tabla, id)
{
    console.log(BaseURL+'/data'+tabla);
    $.ajax({
      url:BaseURL+'/data'+tabla,
      data: {id:id},
      method:'post',
      dataType: 'json',
      headers: {
                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
              },
      success:function(response){
        
        console.log(response.dotacion);

      }
    });
    setTimeout(function() {
      $("#myModal").modal('show');
  }, 300);
}

function fechaCambiada(esto)
{
    var fechaSeleccionada = $(esto).val();
    var id = $('#proyecto_id').val();
    $('#fecha_dotacion').val(fechaSeleccionada);
    $('#fecha_ausentismo').val(fechaSeleccionada);
    $('#fecha_planificacion').val(fechaSeleccionada);
    $('#fecha_hijos').val(fechaSeleccionada);
    
    $.ajax({
      url:BaseURL+'/getTablasUpDown',
      data: {fechaSeleccionada:fechaSeleccionada, proyecto_id:id},
      method:'post',
      dataType: 'json',
      headers: {
                'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
              },
      success:function(response){
        
        
        $.each(response.tablas, function( index, value ) 
        { 
            if (value == false) 
            {
                $('#'+index+'-panel').css('pointer-events', 'none').css('background-color', '#ffd2d2');
            }else{
                $('#'+index+'-panel').css('pointer-events', 'auto').css('background-color', '#e1ffd2')
            }
            
          console.log(index+'  ' +value);
        });

      }
    });
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Initialize popover component
$(function () {
  $('[data-toggle="popover"]').popover()
})
//FILE PRUEBA
