
<?php $__env->startSection('title','Cargar Datos KPI Analitics'); ?>
<?php $__env->startSection('content'); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-title col-sm-12">
            <h5><img width="130px" src="<?php echo e(asset('img/kpi-analytics-icono.png')); ?>" alt="Logo KPI Analytics">KPI Analytics Carga tus datos</h5>
            
        </div>

        <hr class="mb-4">

        <div class="row">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
            <div class="text-center mb-4">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-exclamation-triangle mr-2" aria-hidden="true" style="color: #fffefe;"></i>    
                    Mostrar errores
                </button>
            </div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="form-group pb-3 d-flex justify-content-center col-sm-12">
                        <label for="sub_id" class="text-center col-md-2" style="padding: 1rem;">Fecha de Subida</label>
                        <?php echo Form::select('fecha_up', $fechas, $fecha_mes_pasado, ['class'=>'form-control col-md-10 selectpicker', 'id' => 'fecha_up', 'data-style' => 'btn btnBorder',  'data-live-search' => 'true', 'title' => 'Seleccione fecha', 'data-size' => '6', 'onchange' => 'fechaCambiada(this)']); ?>

                    </div>

                    <div id="dotacion-panel" class="panel" onclick="cambiarIcono('icon-dotacion')">
                        <?php echo Form::open(['route'=> 'dotacion.store', 'method'=>'POST', 'files' => true ]); ?>

                        <?php echo Form::hidden('proyecto_id', $id,['id' => 'proyecto_id']); ?>


                        <?php echo Form::hidden('fecha_up_hidden', $fecha_mes_pasado,['id' => 'fecha_dotacion']); ?>

                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
                                <i id="icon-dotacion" class="more-less fas fa-plus"></i>
                                    <span class="text-accordeon">1. Dotación</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="conten-interior">
                                    <hr class="mb-2">
                                    <div class="out-wrap">
                                        <p class="head">Carga tu data aquí</p>
                                        <div class="form-group">
                                        <label for="exampleFormControlFile1">Seleccione su data</label>
                                         <input type="file" class="form-control-file" name="dotacion_file">
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center pb-3">
                                <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                                <?php echo Form::close(); ?>

                            </div>
                        </div>

                    </div>

                    <div id="ausentismo-panel" class="panel" onclick="cambiarIcono('icon-ausentismo')">
                        <?php echo Form::open(['route'=> 'ausentismo.store', 'method'=>'POST', 'files' => true ]); ?>

                        <?php echo Form::hidden('proyecto_id', $id,['id' => 'proyecto_id']); ?>

                        <?php echo Form::hidden('fecha_up_hidden', $fecha_mes_pasado,['id' => 'fecha_ausentismo']); ?>

                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i id="icon-ausentismo" class="more-less fas fa-plus"></i>
                                    <span class="text-accordeon">2. Ausentismo</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div class="conten-interior">
                                    <hr class="mb-4">
                                        <div class="out-wrap">
                                            <p class="head">Carga tu data aquí</p>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Seleccione su data</label>
                                                <input type="file" class="form-control-file" name="ausentismo_file">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="text-center pb-3">
                                <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>


                    <div id="planificacion-panel" class="panel" onclick="cambiarIcono('icon-planificacion')">

                        <?php echo Form::open(['route'=> 'planificacion.store', 'method'=>'POST', 'files' => true ]); ?>

                        <?php echo Form::hidden('proyecto_id', $id,['id' => 'proyecto_id']); ?>

                        <?php echo Form::hidden('fecha_up_hidden', $fecha_mes_pasado,['id' => 'fecha_planificacion']); ?>


                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i id="icon-planificacion" class="more-less fas fa-plus"></i>
                                    <span class="text-accordeon">3. Planificación</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="conten-interior">
                                     <hr class="mb-4">
                                        <div class="out-wrap">
                                            <p class="head">Carga tu data aquí</p>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Seleccione su data</label>
                                                <input type="file" class="form-control-file" name="planificacion_file">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="text-center pb-3">
                                <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                                <?php echo Form::close(); ?>

                            </div>

                        </div>
                    </div>


                    <div id="hijos-panel" class="panel" onclick="cambiarIcono('icon-hijos')">

                        <?php echo Form::open(['route'=> 'hijos.store', 'method'=>'POST', 'files' => true ]); ?>

                        <?php echo Form::hidden('proyecto_id', $id,['id' => 'proyecto_id']); ?>

                        <?php echo Form::hidden('fecha_up_hidden', $fecha_mes_pasado,['id' => 'fecha_hijos']); ?>


                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i id="icon-hijos" class="more-less fas fa-plus"></i>
                                    <span class="text-accordeon">4. Hijos</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                <div class="conten-interior">
                                     <hr class="mb-4">
                                        <div class="out-wrap">
                                            <p class="head">Carga tu data aquí</p>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Seleccione su data</label>
                                                <input type="file" class="form-control-file" name="hijos_file">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="text-center pb-3">
                                <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>



                    <div id="cursos-panel" class="panel" onclick="cambiarIcono('icon-cursos')">
                        <?php echo Form::open(['route'=> 'cursos.store', 'method'=>'POST', 'files' => true ]); ?>

                        <?php echo Form::hidden('proyecto_id', $id,['id' => 'proyecto_id']); ?>

                        <?php echo Form::hidden('fecha_up_hidden', $fecha_mes_pasado,['id' => 'fecha_hijos']); ?>



                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i id="icon-cursos" class="more-less fas fa-plus"></i>
                                    <span class="text-accordeon">5. Cursos</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                                <div class="conten-interior">
                                     <hr class="mb-4">
                                        <div class="out-wrap">
                                            <p class="head">Carga tu data aquí</p>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Seleccione su data</label>
                                                <input type="file" class="form-control-file" name="cursos_file">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="text-center pb-3">
                                <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                                <?php echo Form::close(); ?>

                            </div>

                        </div>
                    </div>


                    <div id="asistencia-panel" class="panel" onclick="cambiarIcono('icon-asistencias')">
                        <?php echo Form::open(['route'=> 'asistencia.store', 'method'=>'POST', 'files' => true ]); ?>

                        <?php echo Form::hidden('proyecto_id', $id,['id' => 'proyecto_id']); ?>

                        <?php echo Form::hidden('fecha_up_hidden', $fecha_mes_pasado,['id' => 'fecha_hijos']); ?>


                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i id="icon-asistencias" class="more-less fas fa-plus"></i>
                                    <span class="text-accordeon">6. Asistencias</span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                                <div class="conten-interior">
                                     <hr class="mb-4">
                                        <div class="out-wrap">
                                            <p class="head">Carga tu data aquí</p>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Seleccione su data</label>
                                                <input type="file" class="form-control-file" name="asistencia_file">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="text-center pb-3">
                                <?php echo Form::submit('Cargar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>

                </div><!-- panel-group -->
            </div><!-- container -->
        </div>      
    </div>
<?php echo $__env->make('partials.informeErrores', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">
    $( document ).ready(function() {
        $( "#fecha_up" ).trigger( "change" );
        <?php  
            if (Session::has('messageS')) 
                {
                 ?>
                 $("#exampleModal").modal('show');
                 <?php   
                }
        ?>
        
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>