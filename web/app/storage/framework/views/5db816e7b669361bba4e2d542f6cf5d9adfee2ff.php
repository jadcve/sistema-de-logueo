
<?php $__env->startSection('title','Usuario Crear'); ?>
<?php $__env->startSection('content'); ?>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Asignar usuario a <?php echo e($proyecto->pro_nombre); ?></h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                <?php echo Form::open(['route'=> 'usuarios.asignarV', 'method'=>'POST']); ?>


                <?php echo Form::hidden('proyecto_id', $proyecto->id); ?>

                <div class="form-group pb-3">
                    <div class="row">
                        <label for="sub_id" class="col-sm-3">Usuario<strong>*</strong></label>
                        <?php echo Form::select('usu_id[]', $visualizadores, $usuariosPermiso,['class'=>'form-control selectpicker col-sm-9', 'id' => 'sub_id', 'data-style' => 'btn btn-light btnBorder', 'multiple',  'data-live-search' => 'true', 'title' => 'Seleccione usuarios']); ?>

                    </div>
                </div>

                <div class="text-center pb-5">
                    <?php echo Form::submit('Actualizar permisos', ['class' => 'btn btn-primary block full-width m-b']); ?>

                    <?php echo Form::close(); ?>

                </div>

                <div class="text-center texto-leyenda">
                    <p><strong>*</strong> Campos obligatorios</p>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>