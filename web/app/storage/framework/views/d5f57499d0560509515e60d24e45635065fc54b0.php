
<?php $__env->startSection('title','Empresa Crear'); ?>
<?php $__env->startSection('content'); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="<?php echo e(route('empresa.index')); ?>">Empresas</a></li>
    <li class="breadcrumb-item active" aria-current="page">Crear</li>
<?php $__env->stopSection(); ?>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Crear empresa</h5>
            </div>
            <hr class="mb-4">
                <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                    <?php echo Form::open(['route'=> 'empresa.store', 'method'=>'POST']); ?>

                    <div class="form-group">
                       <div class="row">
                            <label for="emp_nombre" class="col-sm-3">Nombre <strong>*</strong></label>
                            <?php echo Form::text('emp_nombre', null, ['placeholder'=>'Nombre de la empresa', 'class'=>'form-control col-sm-9', 'required']); ?>

                       </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="rub_id" class="col-sm-3">Rubro <strong>*</strong></label>
                            <?php echo Form::select('rub_id', $rubro, null,['class'=>'form-control col-sm-9', 'required'=>'required', 'onchange' => 'cambiarSubrubro(this)']); ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="sub_id" class="col-sm-3">Sub rubro <strong>*</strong></label>
                            <?php echo Form::select('sub_id', [], null,['class'=>'form-control col-sm-9', 'required'=>'required', 'id' => 'sub_id']); ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="pai_id" class="col-sm-3">País <strong>*</strong></label>
                            <?php echo Form::select('pai_id', $pais, null,['class'=>'form-control col-sm-9', 'required'=>'required']); ?>

                        </div>
                    </div>
                    <div class="form-group pb-4">
                        <div class="row">
                            <label for="emp_tlf" class="col-sm-3">Teléfono <strong>*</strong></label>
                            <?php echo Form::text('emp_tlf', null, ['placeholder'=>'Telefono', 'class'=>'form-control col-sm-9', 'required'=>'required']); ?>

                        </div>
                    </div>
                    <div class="form-group pb-4">
                            <label for="emp_estado">Estado</label>
                            <?php echo Form::checkbox('emp_estado', 1, null, ['class' => 'form-control col-sm-9', 'data-toggle' => 'toggle', 'data-on' => 'Activo', 'data-off' => 'Inactivo', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'checked', 'data-style' => 'float-right']); ?>

                    </div>
                    <div class="text-center pb-5">
                        <?php echo Form::submit('Registrar empresa', ['class' => 'btn btn-primary block full-width m-b']); ?>

                        <?php echo Form::close(); ?>

                    </div>

                    <div class="text-center texto-leyenda">
                        <p><strong>*</strong> Campos obligatorios</p>
                    </div>
                </div>
            
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>