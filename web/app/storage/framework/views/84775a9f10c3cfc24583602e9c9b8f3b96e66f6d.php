
<?php $__env->startSection('title','Usuario Crear'); ?>
<?php $__env->startSection('content'); ?>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Agregar usuarios</h5>
                <P>Dentro de esta sección podrás agregar usuarios visualizadores, el cual solo podrá visualizar un proyecto no editar.</P>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                <?php echo Form::open(['route'=> 'usuarios.storeV', 'method'=>'POST']); ?>


                <?php echo Form::hidden('proyecto_id', $proyecto_id); ?>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre" class="col-sm-3">Nombre del usuario <strong>*</strong></label>
                        <?php echo Form::text('usu_nombre', null, ['placeholder'=>'Nombre del usuario', 'class'=>'form-control col-sm-9', 'required']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre" class="col-sm-3">Apellido del usuario <strong>*</strong></label>
                        <?php echo Form::text('usu_apellido', null, ['placeholder'=>'Apellido del usuario', 'class'=>'form-control col-sm-9', 'required']); ?>

                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label for="usu_email" class="col-sm-3">Email <strong>*</strong></label>
                        <?php echo Form::text('usu_email', old('email'), ['class'=>'form-control col-sm-9', 'placeholder'=>'Email']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_pass" class="col-sm-3">Contraseña <strong>*</strong></label>
                        <?php echo e(Form::password('usu_pass',array('placeholder'=>'Contraseña','class' => 'form-control col-sm-9','required'))); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_pass_rep" class="col-sm-3">Repita Contraseña <strong>*</strong></label>
                        <?php echo e(Form::password('usu_pass_confirmation',array('placeholder'=>'Repita la contraseña','class' => 'form-control col-sm-9','required'))); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_tlf" class="col-sm-3">Teléfono</label>
                        <?php echo Form::text('usu_tlf', null, ['placeholder'=>'Telefono', 'class'=>'form-control col-sm-9']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre_cargo" class="col-sm-3">Cargo </label>
                        <?php echo Form::text('usu_nombre_cargo', null, ['placeholder'=>'Nombre del cargo', 'class'=>'form-control col-sm-9']); ?>

                    </div>
                </div>

                <div class="text-center pb-5">
                    <?php echo Form::submit('Registrar usuarios', ['class' => 'btn btn-primary block full-width m-b']); ?>

                    <?php echo Form::close(); ?>

                </div>

                <div class="text-center texto-leyenda">
                    <p><strong>*</strong> Campos obligatorios</p>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>