<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();



Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('/');
    Route::any('login', 'Auth\LoginController@login');
     
});

/***Usuarios logueados****/

Route::group(['middleware' => 'auth'], function () {

    Route::any('logout', 'Auth\LoginController@logout');
    Route::get('home', 'HomeController@index')->name('home');


     
});

//Rutas comunes para Evaluados y Evaluadores

Route::group(['middleware' => ['CheckRol:SuperAdministrador']], function () {

    Route::get('preguntas', 'PreguntasController@index')->name('preguntas.index');
    Route::get('preguntas/create/{id}','PreguntasController@create')->name('preguntas.create');
    Route::post('preguntas','PreguntasController@store')->name('preguntas.store');
    Route::get('preguntas/{id}','PreguntasController@show')->name('preguntas.show');



});

//rutas disponibles para el Evaluado
Route::group(['middleware' => ['CheckRol:Adminstrador']], function () {







});


//Ruta para funcionalidad del administrador
Route::group(['middleware' => ['CheckRol:Cliente']], function () {

    Route::get('admin', 'AdministradorController@index')->name('admin.index');

   
});