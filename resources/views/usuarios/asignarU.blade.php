@extends('layouts.app2')
@section('title','Usuario Crear')
@section('content')

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Asignar URL a Usuarios</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                {!! Form::open(['route'=> 'usuarios.asignarU', 'method'=>'POST']) !!}

                {!! Form::hidden('proyecto_id', $proyecto->id) !!}

                <div class="table-responsive">
                    <table class="table table-hover" id="dataTableVisualizador" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>URL</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($visualizadores as $key => $us)
                            
                            <tr>
                                <td>
                                    {!! Form::text('usuarioLock', $us, ['placeholder'=>'Apellido del usuario', 'class'=>'form-control col-sm-9', 'readonly' => 'readonly']) !!}
                                    {!! Form::hidden('visualizador[]', $key) !!}

                                </td>
                                <td>
                                    @php
                                    if(array_key_exists($key, $usuariosUrlArray2))
                                    {
                                        $valor = $usuariosUrlArray2[$key];
                                    }else{
                                        $valor = null;
                                    }
                                    @endphp
                                   
                                    {!! Form::select("usu_id[$key][]", $url, $valor,['class'=>'form-control selectpicker col-sm-9', 'id' => 'sub_id', 'data-style' => 'btn btn-light btnBorder', 'multiple',  'data-live-search' => 'true', 'title' => 'Seleccione usuarios', 'data-size' => '3']) !!}
                                </td>
                            </tr>
                            
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center pb-5">
                    {!! Form::submit('Actualizar permisos', ['class' => 'btn btn-primary block full-width m-b']) !!}
                    {!! Form::close() !!}
                </div>
                </div>

            </div>
        </div>
@stop