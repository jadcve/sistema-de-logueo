<?php

use Illuminate\Database\Seeder;
use App\User;


class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        User::create([
            'name'		=>	'Alain Diaz',
            'usu_rut'  =>  '26506613-5',
            'usu_estado' => '1',
            'rol_id' => '1',
            'email'			=>  'jadcve@gmail.com',
            'password' 		=> bcrypt('123456'),
            'usu_fecha_caducidad' => null,
            'usu_cargo' => 'Desarrollador',
            'usu_perfil' => 'Desarrollador'

        ]);

        
        
    }
}
