
<?php $__env->startSection('title','Pais Crear'); ?>
<?php $__env->startSection('content'); ?>
	<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de Cursos</h4>
            </div>
            <hr class="mb-4">

            <div class="table-responsive">
                <table id="dataTableAusentismo" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>id_curso</th>
                        <th>nombre_curso</th>
                        <th>categoria_curso</th>
                        <th>modalidad_curso</th>
                        <th>monto_curso</th>
                        <th>fecha_inicio</th>
                        <th>fecha_fin</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $cursos_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                        <th><?php echo e($cur->id_curso); ?></th>
                        <th><?php echo e($cur->nombre_curso); ?></th>
                        <th><?php echo e($cur->categoria_curso); ?></th>
                        <th><?php echo e($cur->modalidad_curso); ?></th>
                        <th><?php echo e($cur->monto_curso); ?></th>
                        <th><?php echo e($cur->fecha_inicio); ?></th>
                        <th><?php echo e($cur->fecha_fin); ?></th>
                 
                    </tr>      
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </tbody>
            </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>