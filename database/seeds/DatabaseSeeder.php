<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(CategoriaSeeder::class);
        $this->call(PreguntasSeeder::class);
        $this->call(EvaluadoresSeeder::class);
        $this->call(NotasSeeder::class);
    }

}
