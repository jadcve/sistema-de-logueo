
 <!-- The Modal -->
 <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Detalle</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body overflow-auto">
            <table id="dataTable2" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>id_empleado</th>
                        <th>nombre_empleado</th>
                        <th>genero</th>
                        <th>fecha_nacimiento</th>
                        <th>fecha_egreso</th>
                        <th>causal</th>
                        <th>cargo</th>
                        <th>estamento</th>
                        <th>nivel_1</th>
                        <th>nivel_2</th>
                        <th>nivel_3</th>
                        <th>nivel_4</th>
                        <th>nivel_5</th>
                        <th>segmento_1</th>
                        <th>segmento_2</th>
                        <th>segmento_3</th>
                        <th>segmento_4</th>
                        <th>segmento_5</th>
                        <th>sueldo_base</th>
                        <th>otros_fijos</th>
                        <th>comisiones_real</th>
                        <th>comisiones_target</th>
                        <th>bono_real</th>
                        <th>bono_target</th>
                        <th>colacion</th>
                        <th>movilizacion</th>
                        <th>aguinaldo_18</th>
                        <th>aguinaldo_navidad</th>
                        <th>otros_beneficios_cargo</th>
                        <th>otros_beneficios_persona</th>
                        <th>aportes_empleador</th>
                        <th>finiquito</th>
                        <th>horas_extras</th>
                        <th>horas_monto</th>
                        <th>vacaciones</th>
                        <th>desemp_ppal</th>
                        <th>desemp1</th>
                        <th>desemp2</th>
                        <th>desemp3</th>
                        <th>compe_ppal</th>
                        <th>compe_1</th>
                        <th>compe_2</th>
                        <th>compe_3</th>
                        <th>compe_4</th>
                        <th>compe_5</th>
                        <th>compe_7</th>
                        <th>compe_8</th>
                        <th>compe_9</th>
                        <th>compe_10</th>
                        <th>jefe</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($dotacion_data as $key => $dot)
                        <tr>
                        <th>$dot->id_empleado</th>
                        <th>$dot->nombre_empleado</th>
                        <th>$dot->genero</th>
                        <th>$dot->fecha_nacimiento</th>
                        <th>$dot->fecha_egreso</th>
                        <th>$dot->causal</th>
                        <th>$dot->cargo</th>
                        <th>$dot->estamento</th>
                        <th>$dot->nivel_1</th>
                        <th>$dot->nivel_2</th>
                        <th>$dot->nivel_3</th>
                        <th>$dot->nivel_4</th>
                        <th>$dot->nivel_5</th>
                        <th>$dot->segmento_1</th>
                        <th>$dot->segmento_2</th>
                        <th>$dot->segmento_3</th>
                        <th>$dot->segmento_4</th>
                        <th>$dot->segmento_5</th>
                        <th>$dot->sueldo_base</th>
                        <th>$dot->otros_fijos</th>
                        <th>$dot->comisiones_real</th>
                        <th>$dot->comisiones_target</th>
                        <th>$dot->bono_real</th>
                        <th>$dot->bono_target</th>
                        <th>$dot->colacion</th>
                        <th>$dot->movilizacion</th>
                        <th>$dot->aguinaldo_18</th>
                        <th>$dot->aguinaldo_navidad</th>
                        <th>$dot->otros_beneficios_cargo</th>
                        <th>$dot->otros_beneficios_persona</th>
                        <th>$dot->aportes_empleador</th>
                        <th>$dot->finiquito</th>
                        <th>$dot->horas_extras</th>
                        <th>$dot->horas_monto</th>
                        <th>$dot->vacaciones</th>
                        <th>$dot->desemp_ppal</th>
                        <th>$dot->desemp1</th>
                        <th>$dot->desemp2</th>
                        <th>$dot->desemp3</th>
                        <th>$dot->compe_ppal</th>
                        <th>$dot->compe_1</th>
                        <th>$dot->compe_2</th>
                        <th>$dot->compe_3</th>
                        <th>$dot->compe_4</th>
                        <th>$dot->compe_5</th>
                        <th>$dot->compe_7</th>
                        <th>$dot->compe_8</th>
                        <th>$dot->compe_9</th>
                        <th>$dot->compe_10</th>
                        <th>$dot->jefe</th>
                    </tr>      
                  @endforeach
                    
                </tbody>
            </table>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!-- /The Modal -->