
<?php $__env->startSection('title','Pais Crear'); ?>
<?php $__env->startSection('content'); ?>
	<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de dotacion</h4>
            </div>
            <hr class="mb-4">

            <div class="table-responsive">
                <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>id_empleado</th>
                        <th>nombre_empleado</th>
                        <th>genero</th>
                        <th>fecha_nacimiento</th>
                        <th>fecha_egreso</th>
                        <th>causal</th>
                        <th>cargo</th>
                        <th>estamento</th>
                        <th>nivel_1</th>
                        <th>nivel_2</th>
                        <th>nivel_3</th>
                        <th>nivel_4</th>
                        <th>nivel_5</th>
                        <th>segmento_1</th>
                        <th>segmento_2</th>
                        <th>segmento_3</th>
                        <th>segmento_4</th>
                        <th>segmento_5</th>
                        <th>sueldo_base</th>
                        <th>otros_fijos</th>
                        <th>comisiones_real</th>
                        <th>comisiones_target</th>
                        <th>bono_real</th>
                        <th>bono_target</th>
                        <th>colacion</th>
                        <th>movilizacion</th>
                        <th>aguinaldo_18</th>
                        <th>aguinaldo_navidad</th>
                        <th>otros_beneficios_cargo</th>
                        <th>otros_beneficios_persona</th>
                        <th>aportes_empleador</th>
                        <th>finiquito</th>
                        <th>horas_extras</th>
                        <th>horas_monto</th>
                        <th>vacaciones</th>
                        <th>desemp_ppal</th>
                        <th>desemp1</th>
                        <th>desemp2</th>
                        <th>desemp3</th>
                        <th>compe_ppal</th>
                        <th>compe_1</th>
                        <th>compe_2</th>
                        <th>compe_3</th>
                        <th>compe_4</th>
                        <th>compe_5</th>
                        <th>compe_7</th>
                        <th>compe_8</th>
                        <th>compe_9</th>
                        <th>compe_10</th>
                        <th>jefe</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dotacion_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dot): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                        <th><?php echo e($dot->id_empleado); ?></th>
                        <th><?php echo e($dot->nombre_empleado); ?></th>
                        <th><?php echo e($dot->genero); ?></th>
                        <th><?php echo e($dot->fecha_nacimiento); ?></th>
                        <th><?php echo e($dot->fecha_egreso); ?></th>
                        <th><?php echo e($dot->causal); ?></th>
                        <th><?php echo e($dot->cargo); ?></th>
                        <th><?php echo e($dot->estamento); ?></th>
                        <th><?php echo e($dot->nivel_1); ?></th>
                        <th><?php echo e($dot->nivel_2); ?></th>
                        <th><?php echo e($dot->nivel_3); ?></th>
                        <th><?php echo e($dot->nivel_4); ?></th>
                        <th><?php echo e($dot->nivel_5); ?></th>
                        <th><?php echo e($dot->segmento_1); ?></th>
                        <th><?php echo e($dot->segmento_2); ?></th>
                        <th><?php echo e($dot->segmento_3); ?></th>
                        <th><?php echo e($dot->segmento_4); ?></th>
                        <th><?php echo e($dot->segmento_5); ?></th>
                        <th><?php echo e($dot->sueldo_base); ?></th>
                        <th><?php echo e($dot->otros_fijos); ?></th>
                        <th><?php echo e($dot->comisiones_real); ?></th>
                        <th><?php echo e($dot->comisiones_target); ?></th>
                        <th><?php echo e($dot->bono_real); ?></th>
                        <th><?php echo e($dot->bono_target); ?></th>
                        <th><?php echo e($dot->colacion); ?></th>
                        <th><?php echo e($dot->movilizacion); ?></th>
                        <th><?php echo e($dot->aguinaldo_18); ?></th>
                        <th><?php echo e($dot->aguinaldo_navidad); ?></th>
                        <th><?php echo e($dot->otros_beneficios_cargo); ?></th>
                        <th><?php echo e($dot->otros_beneficios_persona); ?></th>
                        <th><?php echo e($dot->aportes_empleador); ?></th>
                        <th><?php echo e($dot->finiquito); ?></th>
                        <th><?php echo e($dot->horas_extras); ?></th>
                        <th><?php echo e($dot->horas_monto); ?></th>
                        <th><?php echo e($dot->vacaciones); ?></th>
                        <th><?php echo e($dot->desemp_ppal); ?></th>
                        <th><?php echo e($dot->desemp1); ?></th>
                        <th><?php echo e($dot->desemp2); ?></th>
                        <th><?php echo e($dot->desemp3); ?></th>
                        <th><?php echo e($dot->compe_ppal); ?></th>
                        <th><?php echo e($dot->compe_1); ?></th>
                        <th><?php echo e($dot->compe_2); ?></th>
                        <th><?php echo e($dot->compe_3); ?></th>
                        <th><?php echo e($dot->compe_4); ?></th>
                        <th><?php echo e($dot->compe_5); ?></th>
                        <th><?php echo e($dot->compe_7); ?></th>
                        <th><?php echo e($dot->compe_8); ?></th>
                        <th><?php echo e($dot->compe_9); ?></th>
                        <th><?php echo e($dot->compe_10); ?></th>
                        <th><?php echo e($dot->jefe); ?></th>
                    </tr>      
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </tbody>
            </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>