
<?php $__env->startSection('title','Empresa index'); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h4>Lista de proyectos</h4>
                </div>
                <hr class="mb-4">
               <?php if(Auth::user()->oneRol->rol_nombre == 'Administrador' || Auth::user()->oneRol->rol_nombre == 'SuperAdministrador'): ?>
                <div class="col-lg-12 pb-3 pt-2">
                    <a href="<?php echo e(route('proyectos.create')); ?>" class = 'btn btn-primary'>Nuevo proyecto</a>
                </div>
                <?php endif; ?>

                <div class="table-responsive">
                    <table class="table table-hover" id="dataTableAusentismo" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nombre de Enlace</th>
                            <th>Nombre de Archivo</th>
                            <th>Tipo</th>
                            <th>Ver</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $proyectos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><small><?php echo e($p->url_nombre); ?></small></td>
                                <td><small><?php echo e($p->url_path); ?></small></td>
                                <td>

                                    <?php if( $p->url_tipo != null): ?>
                                       <?php if($p->url_tipo == "pdf"): ?><i class="far fa-file-pdf" style="font-size:20px">
                                       <?php elseif($p->url_tipo == "rar"): ?><i class="fa fa-file-archive" style="font-size:20px">
                                       <?php elseif($p->url_tipo == "xlsx"): ?> <i class="far fa-file-excel" style="font-size:20px">
                                       <?php elseif($p->url_tipo == "pptx"): ?><i class="fa fa-file-powerpoint" style="font-size:20px">
                                       <?php elseif($p->url_tipo == "docx"): ?><i class="fa fa-file-word" style="font-size:20px">
                                       <?php else: ?>"<td>NO DEFINIDO</td>"
                                       <?php endif; ?>
                                   <?php else: ?>
                                       <img src="<?php echo e(asset('img/power-bi-icono.svg')); ?>" width="20px">
                                   <?php endif; ?>
                                </td>

                                <td>
                                    <small>
                                        <a href="<?php echo e(route('proyectos.show', Crypt::encrypt($p->id))); ?>" class="btn-empresa"><i class="far fa-eye"></i></a>
                                    </small>
                                <!--
                                <a href = "<?php echo e(route('proyectos.destroy', $p->id)); ?>" onclick="return confirm('¿Esta seguro que desea eliminar este elemento?')" class="btn-empresa"><i class="far fa-trash-alt"></i>
                                </a>
                            </td>
-->
                                </td>
                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>