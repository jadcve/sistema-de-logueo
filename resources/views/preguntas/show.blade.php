@extends('layouts.app2')
@section('title','Empresa index')
@section('content')

    <section class="content">

        <table class ='table table-bordered'>
            <thead class="ibox-title">
                <th><h5>Resumen de evaluación</h5></th>
            </thead>
        </table>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                  
                  <th scope="col">Respuesta 1</th>
                  <th scope="col">Respuesta 2</th>
                  <th scope="col">Respuesta 3</th>
                  <th scope="col">Respuesta 4</th>
                  <th scope="col">Respuesta 5</th>
                  <th scope="col">Respuesta 6</th>
                </tr>
            </thead>
            <tbody>           
                <tr>
                  <td>{!!$evaluacion->respuesta1!!}</td>
                  <td>{!!$evaluacion->respuesta2!!}</td>
                  <td>{!!$evaluacion->respuesta3!!}</td>
                  <td>{!!$evaluacion->respuesta4!!}</td>
                  <td>{!!$evaluacion->respuesta5!!}</td>
                  <td>{!!$evaluacion->respuesta6!!}</td>
                </tr>
                
            </tbody>
        </table>

        <table class ='table table-bordered'>
            <thead class="ibox-title">
                <tr>
                    <th><h5>Promedio:</h5></th>
                    <th><h5>{{$prom[0]->prom}}</h5></th>                    
                </tr>
                
            </thead>
        </table>

@stop
