<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\notificacion;
use App\rol;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm() 
    {
        return view('auth.login'); 
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    public function login(Request $request)
    {
        $usuario = User::where('email', $request->input('email'))->first();

        if ($usuario)
        {
            //dd($usuario);
            $fecha = substr(now(), 0, 19);

            if ($usuario->usu_estado == 1 and ($usuario->usu_fecha_caducidad > $fecha or $usuario->usu_fecha_caducidad == NULL) ) 
            {

                $credenciales = $request->only('email', 'password');

                if (Auth::attempt($credenciales)) 
                {
                    //credenciales correctas

                    Auth::login($usuario, true);

                    Session::put('activo', true);


                    return redirect('home');


                } else {
                    //credenciales incorrectas
                    flash('Datos ingresados no son válidos.')->error();
                    return redirect('/');
                }

            } else {
                flash('Usuario inactivo, por favor contacte con el administrador.')->error();
                return redirect('/');

            }
        }

         else{
            //el email del usuario no se encuentra en la BD
            flash('Datos ingresados no son válidos.')->error();
            return redirect('/');
         }
    }
}
