
<?php $__env->startSection('title','Empresa index'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de proyectos</h4>
            </div>
            <hr class="mb-4">
            <div class="col-lg-12 pb-3 pt-2">
                <a href="<?php echo e(route('proyectos.create')); ?>" class = 'btn btn-primary'>Crear nuevo proyecto</a>   
            </div>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
	                    <tr>
	                        <th>Nombre</th>
                            <th>Tipo de proyecto</th>
	                        <th>Responsable</th>
                            <th>Empresa</th>
                            <th>Fecha de Caducidad</th>
                            <th>Acci&oacute;n</th>
                            <th>Desactivar</th>
	                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $proyectos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><small><?php echo e($p->pro_nombre); ?></small></td>
                            <td><small><?php echo e($p->tpro_nombre); ?></small></td>
                            <td><small><?php echo e($p->name .' '.$p->usu_apellido); ?></small></td>
                            <td><small><?php echo e($p->emp_nombre); ?></small></td>
                            <td><small><?php echo e(substr($p->pro_fecha_caducidad,0,10)); ?></small></td>
                            <td>
                                <small>
                                    <a href="<?php echo e(route('proyectos.lista', Crypt::encrypt($p->id))); ?>" class="btn-empresa"><i class="far fa-eye"></i></a>
                                </small>
                                <small>
                                    <a href="<?php echo e(route('proyectos.edit', Crypt::encrypt($p->id))); ?>" class="btn-empresa"><i class="far fa-edit"></i></a>
                                </small>
<!--
                                <a href = "<?php echo e(route('proyectos.destroy', $p->id)); ?>" onclick="return confirm('¿Esta seguro que desea eliminar este elemento?')" class="btn-empresa"><i class="far fa-trash-alt"></i>
                                </a>
                            </td>
-->

                            <td>
                                <div class="center">
                                    <input type="checkbox" <?php echo e(($p->pro_estado == 1) ? "checked" : ""); ?> data-toggle="toggle" data-on="Activo" data-off="Inactivo" data-onstyle="success" data-offstyle="danger" h="<?php echo e($p->id); ?>" value="<?php echo e($p->pro_estado); ?>" class="desactivarProyectos">
                                </div>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>