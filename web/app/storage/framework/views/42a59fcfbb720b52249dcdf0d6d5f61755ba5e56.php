
<?php $__env->startSection('title','Pais Editar'); ?>
<?php $__env->startSection('content'); ?>

<section class="content">
    <h1>
        <i class="fa fa-exclamation-triangle"aria-hidden="true" style="color: red;"></i> Error
    </h1>
    <div class="row">
        <div class="col-xs-12">
            <h2><?php echo $message; ?></h2>
        </div>
    </div>
    
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>