<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Evaluacion') }}</title>
  <!-- Custom fonts for this template-->
    
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="{{ asset('css/sb-admin-2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">  
  <!-- link para los toggle-->
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">


  <!-- css selectpicker -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">


  
</head>

<body id="page-top">

  <!--definicion de urls y token-->
 <script>
      Base =  {!! json_encode(url('/api')) !!}
      BaseURL =  {!! json_encode(url('/')) !!}
      
  </script>
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- ICONOS -->
  <script src="https://kit.fontawesome.com/8f05940c60.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin-2.js') }}"></script>

  <!-- Page level plugins -->
  <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- link para los toggle-->
  <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>

  <!-- JavaScript selectpicker -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
  
    $(document).ready(function() {
      
      $('.selectpicker').selectpicker();    
      //$('.selectpickerCreateU').selectpicker();

      $('#dataTableVisualizador').DataTable({
        "pageLength": 100,
          "order": [],
          "language": {
          "lengthMenu": "Mostrar _MENU_ registros por pagina",
          "zeroRecords": "No hay datos que coincidan con la busqueda",
          "info": "Mostrando _PAGE_ de _PAGES_ paginas",
          "infoEmpty": "No hay datos disponibles",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "paginate": {
            "previous": "Antes",
            "next": "Siguiente"
          },
          "searchPlaceholder": "Buscador...",
          "sSearch": ""
          },
          "ordering": false,
          "dom": "",
      });
      $('#dataTableVisualizador_filter input').css( "border-radius", "50px" ); 
      $('#dataTableVisualizador_filter input').removeClass( "form-control-sm" );

      $('#dataTableAusentismo').DataTable({
        "pageLength": 100,
          "order": [],
          "language": {
          "lengthMenu": "Mostrar _MENU_ registros por pagina",
          "zeroRecords": "No hay datos que coincidan con la busqueda",
          "info": "Mostrando _PAGE_ de _PAGES_ paginas",
          "infoEmpty": "No hay datos disponibles",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "paginate": {
            "previous": "Antes",
            "next": "Siguiente"
          },
          "searchPlaceholder": "Buscador...",
          "sSearch": ""
          },
          "dom": "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +"<'row'<'col-sm-12'tr>>" + "<'row pt-4'<'col-sm-12 col-md-4'i><'col-sm-12 d-flex justify-content-center col-md-4 col-md-4'p>>",
      });
      $('#dataTableAusentismo_filter input').css( "border-radius", "50px" ); 
      $('#dataTableAusentismo_filter input').removeClass( "form-control-sm" );
      
      $('#dataTable').DataTable({
        "columnDefs": [
          { "width": "16%", "targets": 0 }, 
          { "width": "14%", "targets": 1, "orderable": false },
          { "width": "14%", "targets": 2, "orderable": false },
          { "width": "14%", "targets": 3, "orderable": false },
          { "width": "14%", "targets": 4, "orderable": false },
          { "width": "14%", "targets": 5, "orderable": false },
          { "width": "14%", "targets": 6, "orderable": false }
        ],
        "pageLength": 100,
          "order": [],
          "language": {
          "lengthMenu": "Mostrar _MENU_ registros por pagina",
          "zeroRecords": "No hay datos que coincidan con la busqueda",
          "info": "Mostrando _PAGE_ de _PAGES_ paginas",
          "infoEmpty": "No hay datos disponibles",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "paginate": {
            "previous": "Antes",
            "next": "Siguiente"
          },
          "searchPlaceholder": "Buscador...",
          "sSearch": ""
          },
          "dom": "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +"<'row'<'col-sm-12'tr>>" + "<'row pt-4'<'col-sm-12 col-md-4'i><'col-sm-12 d-flex justify-content-center col-md-4 col-md-4'p>>",
      });
      $('#dataTable_filter input').css( "border-radius", "50px" ); 
      $('#dataTable_filter input').removeClass( "form-control-sm" );
    });

  </script>

  <!--dropzone-->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/dropzone.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.js"></script>

  <!--incluyendo el custom.js-->
  <script src="{{ asset('js/custom.js') }}"></script>

  <!-- Page Wrapper -->
  <div id="wrapper">
  @include('layouts.navbar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
    @include('layouts.topnav')



    @include('layouts.footer')

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade modal-sesion" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
              
          <h5 class="modal-title" id="exampleModalLabel">¿Deseas salir de la plataforma?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body">Selecciona "Cerrar sesión" si estas seguro de cerrar sesion actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="{{ route('logout') }}">Cerrar sesión</a>
        </div>
      </div>
    </div>
  </div>


</body>

</html>
