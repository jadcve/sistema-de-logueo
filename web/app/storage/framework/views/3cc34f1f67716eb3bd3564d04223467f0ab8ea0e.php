
<?php $__env->startSection('title','Usuarios index'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de Usuarios</h4>
            </div>
            <hr class="mb-4">
            <div class="col-lg-12 pb-3 pt-2">
                <a href="<?php echo e(route('usuarios.create')); ?>" class = 'btn btn-primary'>Crear nuevo Usuario</a>        
            </div>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
	                    <tr>
	                        <th>Nombres</th>
                            <th>Correo</th>
	                        <th>Rol</th>
                            <th>Empresa</th>
	                        <th>Acci&oacute;n</th>
                            <th>Desactivar</th>
	                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $us): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <tr>
                            <td><small><?php echo e($us->name . ' ' . $us->usu_apellido); ?></small></td>
                            <td><small><?php echo e($us->email); ?></small></td>
                            <td><small><?php echo e($us->oneRol->rol_nombre); ?></small></td>
                            <td><small><?php echo e($us->oneEmpresa->emp_nombre); ?></small></td>


                            <td>
                                <small>
                                    <a href="<?php echo e(route('usuarios.edit',  Crypt::encrypt($us->id))); ?>" class="btn-empresa"><i class="far fa-edit"></i></a>
                                </small>
<!--
                                <a href = "<?php echo e(route('usuarios.destroy', $us->id)); ?>" onclick="return confirm('¿Esta seguro que desea eliminar este elemento?')" class="btn-empresa"><i class="far fa-trash-alt"></i>
                                </a>
-->
                            </td>
                            <td>
                                <div class="center">
                                    <input type="checkbox" <?php echo e(($us->usu_estado == 1) ? "checked" : ""); ?> data-toggle="toggle" data-on="Activo" data-off="Inactivo" data-onstyle="success" data-offstyle="danger" h="<?php echo e($us->id); ?>" value="<?php echo e($us->usu_estado); ?>" class="desactivarUsuario">

                                </div>
                                
                            </td>
                        </tr>
                        
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>