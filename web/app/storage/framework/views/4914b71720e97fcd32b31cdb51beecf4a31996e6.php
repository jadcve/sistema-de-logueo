
<?php $__env->startSection('title','Empresa Crear'); ?>

<?php $__env->startSection('content'); ?>


    <!--Funcion para ocultar y mostrar input segun seleccion-->
    <script language="javascript" type="text/javascript">
        function d1(selectTag){
            if(selectTag.value == '1'){
                $('#bloque_archivo').show();
                $('#bloque_url').hide();
                $('.contenedorNro').attr('disabled',false);
                //document.getElementsByClassName('contenedorNro').disabled = false;
                document.getElementById('url').disabled = true;
            }else if(selectTag.value == '2'){
                $('#bloque_archivo').hide();
                $('#bloque_url').show();
                //document.getElementsByClassName('contenedorNro').disabled = true;
                $('.contenedorNro').attr('disabled',true);
                document.getElementById('url').disabled = false;
            } else {
                $('#bloque_archivo').hide();
                $('#bloque_url').hide();
            }
        }

        //al terminar de cargar el contenido de esta vista ejecutar el onchange de empresa
        $( document ).ready(function() {
            $( "#emp_id" ).trigger( "change" );
        });

        function addArchivo()
        {
            var num = $('.contenedorNro').length -1;
    
            var lastIdContenedor = $('.contenedorNro').last().attr('h-cont');

            if (lastIdContenedor > num) 
            {
                var newNum =   1 + parseInt(lastIdContenedor);
                
            }else{
                var newNum =  1 + num;
                
            }
            
            var nombreClonado = $('#nombreClonar').clone();
            $("#contenedorFiles").append(nombreClonado);
            
            nombreClonado.attr('id', 'url_nombre-'+newNum).attr('h-cont',+newNum).attr('name','archivo_nombre[]');
            nombreClonado.removeAttr("style");

            var clonado = $('#archivoClonar').clone();
            $("#contenedorFiles").append(clonado);

            clonado.attr('id', 'archivo-'+newNum).attr('h-cont',+newNum).attr('name','path[]');
            clonado.removeAttr("style");

            var btnclonado = $('#btnClonar').clone();
            $("#contenedorFiles").append(btnclonado);

            btnclonado.attr('h-cont',+newNum).attr('onclick','this.blur();eliminarClon(this);');
            btnclonado.removeAttr("style");
        }
        function eliminarClon(obj)
        {
            var hCont = $(obj).attr('h-cont');
            //$('#contenedor-'+hCont).remove();
            $('*[h-cont='+hCont+']').remove();
        }
        function addUrl()
        {
            var num = $('.contenedorNroUrl').length -1;
    
            var lastIdContenedor = $('.contenedorNroUrl').last().attr('h-cont-url');

            if (lastIdContenedor > num) 
            {
                var newNum =   1 + parseInt(lastIdContenedor);
                
            }else{
                var newNum =  1 + num;
                
            }
            
            var nombreClonado = $('#nombreClonar').clone();
            $("#contenedorUrl").append(nombreClonado);
            
            nombreClonado.attr('id', 'url_nombre-'+newNum).attr('h-cont-url',+newNum).attr('name','url_nombre[]');
            nombreClonado.removeAttr("style");

            var clonado = $('#urlClonar').clone();
            $("#contenedorUrl").append(clonado);

            clonado.attr('id', 'url-'+newNum).attr('h-cont-url',+newNum).attr('name','url[]');
            clonado.removeAttr("style");

            var btnclonado = $('#btnClonar').clone();
            $("#contenedorUrl").append(btnclonado);

            btnclonado.attr('h-cont-url',+newNum).attr('onclick','this.blur();eliminarClonUrl(this);');
            btnclonado.removeAttr("style");
        }
        function eliminarClonUrl(obj)
        {
            var hCont = $(obj).attr('h-cont-url');
            //$('#contenedor-'+hCont).remove();
            $('*[h-cont-url='+hCont+']').remove();
        }
    </script>
    <!--Fin Funcion para ocultar y mostrar input segun seleccion-->

    <!--CCS para ocultar div antes de seleccionar-->
    <style type="text/css">
        .bloque_archivo{
            display:none;
        }
        .bloque_url{
            display:none;
        }

    </style>
    <!--FIN CCS para ocultar div antes de seleccionar-->


        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Crear Proyecto</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                <?php echo Form::open(['route'=> 'proyectos.store', 'method'=>'POST', 'files' => true]); ?>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Nombre del Proyecto <strong>*</strong></label>
                        <?php echo Form::text('pro_nombre', null, ['placeholder'=>'Nombre del proyecto', 'class'=>'form-control col-sm-9', 'required']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Tipo de Proyecto <strong>*</strong></label>
                        <?php echo Form::select('tpro_id', $tpro, null,['class'=>'form-control col-sm-9', 'required'=>'required']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Empresa <strong>*</strong></label>
                        <?php echo Form::select('emp_id', $empresas, null,['class'=>'form-control col-sm-9', 'id' => 'emp_id', 'required'=>'required', 'onchange' => 'cambiarResponsable(this)']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                    <label class="col-sm-3">Responsable <strong>*</strong></label>
                        <?php echo Form::select('usu_id', [], null,['class'=>'form-control col-sm-9', 'id' => 'res_id', 'required'=>'required']); ?>

                    </div>
                </div>

                <!--<div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Tipo de Documento </label>
                        <select name="tipo" onchange="d1(this)" class="form-control col-sm-9">
                            <option value="0">Seleccionar</option>
                            <option value="1">Archivo</option>
                            <option value='2'>URL</option>
                        </select>
                        </div>
                </div>-->  

                <?php echo Form::text('', null, ['id' => 'nombreClonar', 'autocomplete' => 'off', 'placeholder'=>'Nombre del enlace', 'class'=>'form-control col-sm-4 mb-2 mr-4', 'style' => 'display: none !important;']); ?>


                <input style="display: none !important;" id="archivoClonar" type='file' class="form-control col-sm-6 contenedorNro mb-2">
                
                <button style="display: none !important;" id="btnClonar" type="button" class="form-control btn col-sm-1 eliminarClonFlag mb-2"><i style="font-size: 1.8rem;color:#e74a3b;" class="fas fa-minus-circle"></i></button>
                
                <input style="display: none !important;" type='text' id="urlClonar" class="form-control col-sm-6 contenedorNroUrl mb-2 ">


                <div id="bloque_archivo" class="form-group">
                    <div class="row">

                        <label class="col-sm-2">Archivo </label>
                        <button type="button" class="btn form-control col-sm-1" onclick="this.blur();addArchivo()"><i class="fas fa-plus-circle" style="font-size: 2rem;color:#1cc88a;"></i></button>

                        <div id="contenedorFiles" title="Archivos admitidos: pdf, xlsx, pptx, docx, rar - Tamaño máximo 100 Megas" class="form-row col-sm-9">

                            <?php echo Form::text('archivo_nombre[]', null, ['autocomplete' => 'off','placeholder'=>'Nombre del enlace', 'class'=>'form-control mr-4 col-sm-4 mb-2', 'h-cont' => '1']); ?>


                            <input type='file' id="archivo-1" name='path[]' class="form-control col-sm-6 contenedorNro mb-2" h-cont="1">

                            <button type="button" class="form-control btn col-sm-1 eliminarClonFlag mb-2" h-cont="1" onclick="this.blur();eliminarClon(this)" ><i class="fas fa-minus-circle" style="font-size: 1.8rem;color:#e74a3b;"></i></button>

                        </div>
                    </div>
                </div>       
                       
                <div id="bloque_url" class="form-group">
                    <div class="row">

                            <label class="col-sm-2">URL </label>
                            <button type="button" class="btn form-control col-sm-1" onclick="this.blur();addUrl()"><i class="fas fa-plus-circle" style="font-size: 2rem;color:#1cc88a;"></i></button>

                            <div id="contenedorUrl" class=" form-row col-sm-9">

                                <?php echo Form::text('url_nombre[]', null, ['placeholder'=>'Nombre del enlace', 'class'=>'form-control col-sm-4 mr-4 mb-2', 'h-cont-url' => '1']); ?>


                                <input type='text' id="url-1" name='url[]' class="form-control col-sm-6 contenedorNroUrl mb-2" h-cont-url="1">

                                <button type="button" class="form-control btn col-sm-1 eliminarClonFlag mb-2" h-cont-url="1" onclick="this.blur();eliminarClonUrl(this)" ><i style="font-size: 1.8rem;color:#e74a3b;" class="fas fa-minus-circle"></i></button>

                            </div>
                    </div>
                </div>
                        
                <!--<div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Nombre del Enlace </label>
                        <?php echo Form::text('url_nombre', null, ['placeholder'=>'Nombre del enlace', 'class'=>'form-control col-sm-9', 'required']); ?>

                    </div>
                </div>-->


                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Fecha Caducidad <strong>*</strong></label>
                        <input type="date" name="fecha_caducidad" id="date" class="form-control col-sm-9"/>
                    </div>
                </div>

                <div class="form-group pb-4">
                      <?php echo Form::label('Estado'); ?>

                      <?php echo Form::checkbox('pro_estado', 1, null, ['class' => 'form-control', 'data-toggle' => 'toggle', 'data-on' => 'Activo', 'data-off' => 'Inactivo', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'checked', 'data-style' => 'float-right']); ?>

                </div>





                <div class="text-center pb-5">
                    <?php echo Form::submit('Registrar Proyecto', ['class' => 'btn btn-primary block full-width m-b']); ?>

                    <?php echo Form::close(); ?>

                </div>

                <div class="text-center texto-leyenda">
                    <p><strong>*</strong> Campos obligatorios</p>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>



<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>