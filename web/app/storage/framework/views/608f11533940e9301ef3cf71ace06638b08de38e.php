
<?php $__env->startSection('title','Empresa index'); ?>
<?php $__env->startSection('content'); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Empresas</li>
<?php $__env->stopSection(); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de empresas</h4>
            </div>
            <hr class="mb-4">
            <div class="col-lg-12 pb-3 pt-2">
                <a href="<?php echo e(route('empresa.create')); ?>" class = 'btn btn-primary'>Crear nueva Empresa</a>        
            </div>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
	                    <tr>
	                        <th>Empresa</th>
                            <th>Rubro</th>
                            <th>SubRubro</th>
	                        <th>Telefono</th>
                            <th>Pais</th>
	                        <th>Acci&oacute;n</th>
                            <th>Desactivar</th>
	                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $empresa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <tr>
                            <td><small><?php echo e($emp->emp_nombre); ?></small></td>
                            <td><small><?php echo e($emp->oneSubRubro->oneRubro->rub_nombre); ?></small></td>
                            <td><small><?php echo e($emp->oneSubRubro->sub_nombre); ?></small></td>
                            <td><small><?php echo e($emp->emp_tlf); ?></small></td>
                            <td><small><?php echo e($emp->onePais->pai_nombre); ?></small></td>
                            <td>
                                <small>
                                    <a href="<?php echo e(route('empresa.edit', Crypt::encrypt($emp->id))); ?>" class="btn-empresa"  title="Editar"><i class="far fa-edit"></i></a>
                                </small>
<!--
                                <a href = "<?php echo e(route('empresa.destroy', $emp->id)); ?>" onclick="return confirm('¿Esta seguro que desea eliminar este elemento?')" class="btn-empresa"><i class="far fa-trash-alt"></i></a>
-->
                            </td>
                            <td>
                                <div class="center">
                                    <input type="checkbox" <?php echo e(($emp->emp_estado == 1) ? "checked" : ""); ?> data-toggle="toggle" data-on="Activo" data-off="Inactivo" data-onstyle="success" data-offstyle="danger" h="<?php echo e($emp->id); ?>" value="<?php echo e($emp->emp_estado); ?>" class="desactivar">

                                </div>
                                
                            </td>
                        </tr>
                        
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>