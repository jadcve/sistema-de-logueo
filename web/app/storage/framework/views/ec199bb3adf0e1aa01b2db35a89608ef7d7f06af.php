
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-exclamation-triangle"aria-hidden="true" style="color: red;"></i>
        KPI Estudios - Informe de errores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body contenido-body">
              
              <section class="content">
                  <div class="row">
                      <div class="col-md-12">
                          <p><?php echo (Session::has('messageS')) ? Session::get('messageS') : $message; ?></p>
                      </div>
                  </div>
              </section>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

