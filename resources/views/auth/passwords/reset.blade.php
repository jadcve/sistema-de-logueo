@extends('layouts.app')
@section('title','KPI Estudios')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 caja-recuperar">
            <div class="card caja-login2">
                <div class="circle-logo2">
                  <!--  <img src="{{ asset('img/Logo-KPI-Estudios-sin-fondo.png') }}"> -->
                </div>
                <div class="card-header text-center">{{ __('Restablecer contraseña') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <div class="alert alert-warning" role="alert">
                                        El correo electronico es invalido.
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <div class="alert alert-warning" role="alert">
                                        Las contraseñas no coinciden.
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-recuperacion">
                                    Cambiar Contraseña
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <p class="text-center">© {{ date('Y') }} {{ config('app.name') }}. @lang('Todos los derechos reservados.')</p>
            </div>
        </div>
    </div>
</div>
@endsection
