
<?php $__env->startSection('title',' KPI Estudios'); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 caja-recuperar">
            <div class="card caja-login2">
                <div class="circle-logo2">
                    <img src="<?php echo e(asset('img/Logo-KPI-Estudios-sin-fondo.png')); ?>">
                </div>
                
                <div class="card-header text-center"><?php echo e(__('Recupere la clave de ingreso a nuestra plataforma')); ?></div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    <form method="POST" action="<?php echo e(route('password.email')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-form-label text-md-center"><?php echo e(__('Ingrese su correo')); ?></label>

                            <div class="col-md-12 text-center">
                                <input id="email" type="email" class="formulario form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required>

                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>No podemos encontrar un usuario con esa dirección de correo electrónico.</strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-recuperacion">
                                    <?php echo e(__('Enviar enlace de restablecimiento de su clave')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                    <script language="JavaScript" type="text/javascript">
                    <!--
                    //This script was found at the JavaScript Place. http://www.javaplace.co.uk
                    var height=0; var width=0;
                    if (self.screen) {     // for NN4 and IE4
                        width = screen.width;
                        height = screen.height
                    }
                    else
                    if (self.java) {   // for NN3 with enabled Java
                        var jkit = java.awt.Toolkit.getDefaultToolkit();
                        var scrsize = jkit.getScreenSize();
                        width = scrsize.width;
                        height = scrsize.height; }
                    document.writeln("<center>")
                    alert(width);
                   
                </script>
                </div>
                <p class="text-center">Copyright © KPI Estudios 2019</p>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>