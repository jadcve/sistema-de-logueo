
<?php $__env->startSection('title','Usuario Crear'); ?>
<?php $__env->startSection('content'); ?>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Crear usuarios</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                <?php echo Form::open(['route'=> 'usuarios.store', 'method'=>'POST']); ?>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre" class="col-sm-3">Nombre del usuario <strong>*</strong></label>
                        <?php echo Form::text('usu_nombre', null, ['placeholder'=>'Nombre del usuario', 'class'=>'form-control col-sm-9', 'required']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre" class="col-sm-3">Apellido del usuario <strong>*</strong></label>
                        <?php echo Form::text('usu_apellido', null, ['placeholder'=>'Apellido del usuario', 'class'=>'form-control col-sm-9', 'required']); ?>

                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label for="usu_email" class="col-sm-3">Email <strong>*</strong></label>
                        <?php echo Form::text('usu_email', old('email'), ['class'=>'form-control col-sm-9', 'placeholder'=>'Email']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                    <label class="col-sm-3">Contraseña <strong>*</strong></label>
                    <input id="password" type="password" class="form-control col-sm-9<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>
                    <?php if($errors->has('password')): ?>
                        <span class="invalid-feedback" role="alert">
                            <script><?php echo e($errors->first('password')); ?></script>
                        </span>
                    <?php endif; ?>
                    </div>

                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="password-confirm" class="col-sm-3">Repita Contraseña <strong>*</strong></label>
                        <input id="password-confirm" type="password" class="form-control col-sm-9" name="password_confirmation" required>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label for="emp_id" class="col-sm-3">Empresa <strong>*</strong></label>
                        <?php echo Form::select('emp_id', $empresas, null,['class'=>'form-control col-sm-9', 'required'=>'required', 'onchange' => 'cambiarSubrubro(this)']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_tlf" class="col-sm-3">Teléfono </label>
                        <?php echo Form::text('usu_tlf', null, ['placeholder'=>'Telefono', 'class'=>'form-control col-sm-9']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre_cargo" class="col-sm-3">Cargo </label>
                        <?php echo Form::text('usu_nombre_cargo', null, ['placeholder'=>'Nombre del cargo', 'class'=>'form-control col-sm-9']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3">Rol <strong>*</strong></label>
                        <?php echo Form::select('rol_id', $roles, null, ['class'=>'form-control col-sm-9', 'required'=>'required']); ?>

                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <label for="rol_id" class="col-sm-3">Notificacion inicio de sesion <strong>*</strong></label>
                        
                        <?php echo Form::select('not_email[]', $usuarios, null,['class'=>'form-control selectpicker col-sm-9', 'data-style' => 'btn btnBorder', 'multiple',  'data-live-search' => 'true', 'title' => 'Seleccione usuarios', 'data-actions-box' => 'true']); ?>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3">Fecha Caducidad </label>
                        <input type="date" name="fecha_caducidad" id="fecha" class="form-control col-sm-9"/>
                    </div>
                </div>

                <div class="form-group">
                      <label for="emp_estado">Estado <strong>*</strong></label>
                        <?php echo Form::checkbox('usu_estado', 1, null, ['class' => 'form-control col-sm-9', 'data-toggle' => 'toggle', 'data-on' => 'Activo', 'data-off' => 'Inactivo', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'checked', 'data-style' => 'float-right']); ?>

                 </div>



                <div class="text-center pb-5">
                    <?php echo Form::submit('Registrar usuarios', ['class' => 'btn btn-primary block full-width m-b']); ?>

                    <?php echo Form::close(); ?>

                </div>

                <div class="text-center texto-leyenda">
                    <p><strong>*</strong> Campos obligatorios</p>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>