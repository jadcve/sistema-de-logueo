<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="{{ asset('img/logo-favicon.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="https://kit.fontawesome.com/8f05940c60.js"></script>
    <title>KPI Estudios</title>
</head>
<body>


<section class="img-fondo">
    <div class="container">
        <div class="row">
            <div class="flash-login">
                        <div class="flash-login-inside">
                          @include('flash::message')
                        </div>
                    </div>
            <div id="panel" class="caja-login text-center">

                <div class="circle-logo">
                  <!--  <img src="img/Logo-KPI-Estudios-sin-fondo.png">  -->
                </div>
                <h2>Bienvenido a nuestra plataforma</h2>

                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf

                    <div class="group">
                        <label>{{ __('Correo') }}</label>
                        <input id="email" type="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="off" wtx-context="4F85AF93-9470-40FE-89CC-6BC260E2495B">
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="group">
                        <label>{{ __('Clave') }}</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" autocomplete="off" name="password" wtx-context="CDC50C64-39C0-44FB-8706-E8A76DB5E32A">
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    

                    <div class="form-group row mb-0 justify-content-center">
                        <div class="">
                            <button id="loginButton" type="submit" class="btn btn-primary">
                                {{ __('Ingresar') }}
                            </button>

                        </div>
                    </div>



                    <div class="contraseña">
                        <a class="contraseña-restablecer" href="{{ route('password.request') }}">¿Olvido su contraseña?</a>
                        <p class="mt-3">© {{ date('Y') }} {{ config('app.name') }}. @lang('Derechos reservados.')</p>
                    </div>

                </form>
               
            </div>
        </div>
    </div>

<script>

    $('#loginButton').on('click', function() {
        $('#loginButton').text("");
        $('#loginButton').append("<i class='fa fa-spinner fa-spin'></i> Ingresando...");
        $('#loginButton').css('pointer-events', 'none');      
        
    });

    $('div.alert').not('.alert-important').delay(3000).fadeTo(350, 0, function(){
       $('div.alert').css("visibility", "hidden");   
    });
   
</script>
</section>


