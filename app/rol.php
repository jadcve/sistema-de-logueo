<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class rol extends Model
{
    use SoftDeletes;
    protected $rol = ['deleted_at'];
    protected $table = 'roles';
}
