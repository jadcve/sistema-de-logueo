@extends('layouts.app')
@section('title',' KPI Estudios')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 caja-recuperar">
            <div class="card caja-login2">
                <div class="circle-logo2">
                  <!-- <img src="{{ asset('img/Logo-KPI-Estudios-sin-fondo.png') }}"> -->
                </div>
                
                <div class="card-header text-center">{{ __('Recupere la clave de ingreso a nuestra plataforma') }}</div>

                <div class="card-body">
                    

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-form-label text-md-center">{{ __('Ingrese su correo') }}</label>

                            <div class="col-md-12 text-center">
                                <input id="email" type="email" class="formulario form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <div class="alert alert-warning" role="alert">
                                        No podemos encontrar un usuario con esa dirección de correo electrónico.
                                    </div>
                                    
                                @endif
                            </div>
                        </div>
                        @if (session('status'))
                        <div class="alert alert-success mt-4" role="alert">
                             Correo enviado exitosamente.
                        </div>
                        @endif
                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-recuperacion">
                                    {{ __('Enviar enlace de restablecimiento de su clave') }}
                                </button>
                            </div>
                        </div>
                    </form>
                        
                </div>
                <p class="text-center">© {{ date('Y') }} {{ config('app.name') }}. @lang('Todos los derechos reservados.')</p>
            </div>
        </div>
    </div>
</div>

@endsection

