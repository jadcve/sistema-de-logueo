
<?php $__env->startSection('title','Rubro Crear'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title titulo-configuracion">
                <h5>Crear rubro</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-6 offset-sm-3 mt-4">
                <?php echo Form::open(['route'=> 'rubro.store', 'method'=>'POST']); ?>

                <div class="form-group justify-content-center">
                    <?php echo Form::label('Nombre del Rubro'); ?>

                    <?php echo Form::text('rub_nombre', null, ['placeholder'=>'Rubro', 'class'=>'form-control', 'required']); ?>

                </div>
                <div class="text-center">
                    <?php echo Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b', 'required']); ?>

                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>