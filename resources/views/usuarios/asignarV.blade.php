@extends('layouts.app2')
@section('title','Usuario Crear')
@section('content')

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Asignar usuario a {{ $proyecto->pro_nombre }}</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                {!! Form::open(['route'=> 'usuarios.asignarV', 'method'=>'POST']) !!}

                {!! Form::hidden('proyecto_id', $proyecto->id) !!}
                <div class="form-group pb-3">
                    <div class="row">
                        <label for="sub_id" class="col-sm-3">Usuario<strong>*</strong></label>
                        {!! Form::select('usu_id[]', $visualizadores, $usuariosPermiso,['class'=>'form-control selectpicker col-sm-9', 'id' => 'sub_id', 'data-style' => 'btn btn-light btnBorder', 'multiple',  'data-live-search' => 'true', 'title' => 'Seleccione usuarios']) !!}
                    </div>
                </div>

                <div class="text-center pb-5">
                    {!! Form::submit('Actualizar permisos', ['class' => 'btn btn-primary block full-width m-b']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="text-center texto-leyenda">
                    <p><strong>*</strong> Campos obligatorios</p>
                </div>
            </div>
        </div>
@stop

