
<?php $__env->startSection('title','Empresa index'); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h4>Bitacora</h4>
                </div>
                <hr class="mb-4">

                <div class="table-responsive">
                    <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Operación</th>
                            <th>Usuario</th>
                            <th>Fecha</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $bitacora; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>
                                <td><?php echo e($bit->bit_transaccion); ?></td>
                                <td><?php echo e($bit->oneUser->name); ?> <?php echo e($bit->oneUser->usu_apellido); ?></td>
                                <td><?php echo e($bit->bit_fecha); ?></td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>