@extends('layouts.app2')
@section('title','Cuestionario Crear')
@section('content')

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cuestionario</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                {!! Form::open(['route'=> 'preguntas.store', 'method'=>'POST']) !!}

                {{ Form::hidden('evaluado_id', $eval_id) }}
                
                @if($perfil->usu_cargo =='Programador')

                <div class="alert alert-primary" role="alert">
                    Categoría 1 - Programadores
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 1</label>
                        {!! Form::select('pregunta1', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 2</label>
                        {!! Form::select('pregunta2', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 3</label>
                        {!! Form::select('pregunta3', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>


                <div class="alert alert-warning" role="alert">
                    Categoría 2 - Programadores
                </div>
               

                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 4</label>
                        {!! Form::select('pregunta4', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 5</label>
                        {!! Form::select('pregunta5', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 6</label>
                        {!! Form::select('pregunta6', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                @endif

                @if($perfil->usu_cargo =='Analista')
                
                
                <div class="alert alert-danger" role="alert">
                    Categoría 1 - Analistas
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 1</label>
                        {!! Form::select('pregunta1', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 2</label>
                        {!! Form::select('pregunta2', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 3</label>
                        {!! Form::select('pregunta3', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>


                <div class="alert alert-success" role="alert">
                    Categoría 2 - Analistas
                </div>
               

                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 4</label>
                        {!! Form::select('pregunta4', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 5</label>
                        {!! Form::select('pregunta5', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="rol_id" class="col-sm-3"> Pregunta 6</label>
                        {!! Form::select('pregunta6', $notas, null, ['class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>
                
                @endif



                <div class="text-center pb-5">
                    {!! Form::submit('Enviar Evaluación', ['class' => 'btn btn-primary block full-width m-b']) !!}
                    {!! Form::close() !!}
                </div>


            </div>
        </div>
@stop



