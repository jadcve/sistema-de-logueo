
<?php $__env->startSection('title','Pais Crear'); ?>
<?php $__env->startSection('content'); ?>
	<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de Planificacion</h4>
            </div>
            <hr class="mb-4">

            <div class="table-responsive">
                <table id="dataTableAusentismo" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>nivel_1</th>
                        <th>cargo</th>
                        <th>dot_planif</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $planificacion_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pla): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                        <th><?php echo e($pla->nivel_1); ?></th>
                        <th><?php echo e($pla->cargo); ?></th>
                        <th><?php echo e($pla->dot_planif); ?></th>
                 
                    </tr>      
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </tbody>
            </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>