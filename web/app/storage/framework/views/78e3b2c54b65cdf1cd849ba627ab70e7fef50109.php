<!-- Main Content -->
<div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>
        
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

        

            <div class="topbar-divider d-none d-sm-block"></div>

            <li class="nav-item dropdown no-arrow mt-4">
                    <span class="mr-2 d-lg-inline text-gray-600 small"><?php echo e(Auth::user()->name.' '.Auth::user()->usu_apellido); ?></span> 
                    <span class="d-none d-lg-inline">|</span>
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo e(Auth::user()->oneEmpresa->emp_nombre); ?></span> 
                    <span class="d-none d-lg-inline">|</span>
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo e(Auth::user()->oneRol->rol_nombre); ?></span>
            </li>
            <!-- Nav Item - salir de sesión -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="modal" data-target="#logoutModal" id="userDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">Cerrar Sesión</span><i class="fas fa-power-off"></i>
                </a>
            </li>

        </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
        <?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
        <!-- Breadcrumbs
        <ol class="breadcrumb">
          <?php echo $__env->yieldContent('breadcrumb'); ?>
        </ol>-->
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->yieldContent('scripts'); ?>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->