<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            if ($exception->getStatusCode() == 404) {
                $errores = '404';
                return response()->view('error.' . '404',  compact('errores'), 404);
            }elseif ($exception->getStatusCode() == 501) {
                # code...
            }
            elseif ($exception->getStatusCode() == 403) {
                    $errores = '403';
                    return response()->view('error.' . '403',  compact('errores'), 403);
            }
            elseif($exception->getStatusCode() == 419) {
                    $errores = '419';
                    return response()->view('error.' . '419',  compact('errores'), 419);
            }
            elseif($exception->getStatusCode() == 429) {
                    $errores = '429';
                    return response()->view('error.' . '429',  compact('errores'), 429);
            }
            elseif($exception->getStatusCode() == 500) {
                    $errores = '500';
                    return response()->view('error.' . '500',  compact('errores'), 500);
            }
            elseif($exception->getStatusCode() == 503) {
                    $errores = '503';
                    return response()->view('error.' . '503',  compact('errores'), 503);
            }elseif($exception->getStatusCode() == 401) {
                
                $errores = '401';
                return response()->view('error.' . '401',  compact('errores'), 401);

            }
            
            
        }
        return parent::render($request, $exception);
    }
}
