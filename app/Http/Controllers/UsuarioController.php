<?php

namespace App\Http\Controllers;

use App\empresa;
use App\User;
use Illuminate\Http\Request;
use App\rol;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\usuario_proyecto;
use App\bitacora;
use App\proyecto;
use View;
use App\notificacion;
use App\Http\Middleware\PreventBackHistory;
use App\Http\Middleware\CheckSession;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\CrearUsuarioRequest;
use App\Http\Requests\EditarUsuarioRequest;


class UsuarioController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(PreventBackHistory::class);
        $this->middleware(CheckSession::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = rol::all()->pluck('rol_nombre','id');
        $empresas = empresa::orderBy('id','desc')->pluck('emp_nombre','id');
        $usuarios = User::select(DB::raw("email, CONCAT(name, ' ', usu_apellido) as nombreApellido"))
                    ->where('rol_id', 1)
                    ->orWhere('rol_id', 2)
                    ->pluck('nombreApellido','email');
        return view('usuarios.create',compact('empresas', 'roles', 'usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CrearUsuarioRequest $request)
    {
       // dd($request);

        $validate = DB::table('users')->where('email', $request->usu_email)->exists();

        if($validate == true)
        {
            flash('El usuario '.$request->usu_email.'  ya existe en la base de datos')->warning();
            return redirect('/usuarios');
        } else
       
        DB::beginTransaction();
        try {
            $usuario = new User();
            $usuario->name = $request->usu_nombre;
            $usuario->usu_apellido = $request->usu_apellido;
            $usuario->password = bcrypt($request->usu_pass);
            $usuario->email = $request->usu_email;
            $usuario->emp_id = $request->emp_id;
            $usuario->usu_tlf = $request->usu_tlf;
            $usuario->rol_id = $request->rol_id;
            $usuario->usu_nombre_cargo = $request->usu_nombre_cargo;
            $usuario->usu_fecha_caducidad = $request->fecha_caducidad;

            if ($request->usu_estado == 1)
            {
                $usuario->usu_estado = $request->usu_estado;
            }else{
                $usuario->usu_estado = 0;
            }

            $usuario->save();
            
            $bit = new bitacora();
            $bit->bit_transaccion = "Se creo $request->usu_nombre $request->usu_apellido como nuevo usuario del sistema";
            $bit->bit_fecha = now();
            $bit->usu_id = Auth::user()->id;
            $bit->save();

            //guardar usuario a quien se notificara el inicio de sesion
            if ($request->not_email != null)
            {
                foreach ($request->not_email as $key => $mail) 
                {
                    $notificacion = new notificacion();
                    $notificacion->not_email = $mail;
                    $notificacion->usu_id = $usuario->id;
                    $notificacion->save();
                }
                
            }
            
            DB::commit();
            // inicio del envio de email//
            
            if ($request->rol_id == 1) 
            {
                $destinatariosExists = User::where('rol_id', 1)->where('id', '!=', $usuario->id)->exists();
                if ($destinatariosExists == true) 
                {
                    $destinatarios = User::where('rol_id', 1)->where('id', '!=', $usuario->id)->pluck('email');
                }

            }elseif ($request->rol_id == 2) {
                
                $destinatariosExists = User::where('rol_id', 1)->where('id', '!=', $usuario->id)->orWhere('rol_id', 2)->exists();
                if ($destinatariosExists == true) 
                {
                     $destinatarios = User::where('rol_id', 1)->where('id', '!=', $usuario->id)->orWhere('rol_id', 2)->pluck('email');
                }

            }elseif ($request->rol_id == 3) {
                
                
                $destinatariosExists = User::where('rol_id', 2)->where('id', '!=', $usuario->id)->exists();
                if ($destinatariosExists == true) 
                {
                    $destinatarios = User::where('rol_id', 2)->where('id', '!=', $usuario->id)->pluck('email');
                }    
            }else{
                $destinatarios = array();
                $destinatariosExists = false;
            }
            
            if ($destinatariosExists == true) 
            {
                $subject = 'Usuario creado exitosamente';
                $rol = rol::where('id', $request->rol_id)->first(['rol_nombre']);
                $texto = 'Usuario <br> '.$request->usu_email.' <br> creado con éxito';
                $nombreCompleto = $request->usu_nombre.' '.$request->usu_apellido; 

                $body = tranformBody($texto, $nombreCompleto, $request->usu_email, $rol->rol_nombre);
                helperSendMail($subject, $body, $destinatarios, $request->usu_email);
            }else{
                flash('Fallo al enviar el email.')->error();
            }
            // inicio del envio de email//
            flash('El usuario ha sido creado correctamente.')->success();
            return redirect('/usuarios');

        }catch (\Exception $e) {

            DB::rollback();
            flash('Error al crear usuario.')->error();
            //flash($e->getMessage())->error();
            return redirect('/usuarios');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id =  Crypt::decrypt($id);
        $usuario = User::where('id', $id)->first();
        $empresas = empresa::orderBy('id','desc')->pluck('emp_nombre','id');
        $roles = rol::all()->pluck('rol_nombre','id');
        $usuarios = User::select(DB::raw("email, CONCAT(name, ' ', usu_apellido) as nombreApellido"))
                    ->where('rol_id', 1)
                    ->orWhere('rol_id', 2)
                    ->pluck('nombreApellido','email');

        $notificaciones = notificacion::where('usu_id', $id)->pluck('not_email');
                    
        return view('usuarios.edit',compact('usuario', 'empresas', 'roles', 'usuarios', 'notificaciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditarUsuarioRequest $request, $id)
    {

        DB::beginTransaction();
        try {  
            $usuario = User::where('id', $id)->first();
            $usuario->name = $request->usu_nombre;
            $usuario->usu_apellido = $request->usu_apellido;
            if ($request->password != null) 
            {
                $usuario->password = bcrypt($request->password);
            }
            $usuario->email = $request->usu_email;
            $usuario->emp_id = $request->emp_id;
            $usuario->usu_tlf = $request->usu_tlf;
            $usuario->rol_id = $request->rol_id;
            $usuario->usu_nombre_cargo = $request->usu_nombre_cargo;
            $usuario->usu_fecha_caducidad = $request->fecha_caducidad;

            if ($request->usu_estado == 1)
            {
                $usuario->usu_estado = $request->usu_estado;
            }else{
                $usuario->usu_estado = 0;
            }
            
            $usuario->update();

            $bit = new bitacora();
            $bit->bit_transaccion = "Se Actualizo al usuario $request->usu_nombre $request->usu_apellido  en la base de datos";
            $bit->bit_fecha = now();
            $bit->usu_id = Auth::user()->id;
            $bit->save();

            //guardar usuario a quien se notificara el inicio de sesion
            $notificacionOld = notificacion::where('usu_id', $usuario->id)->delete();

            if ($request->not_email != null)
            {   
                foreach ($request->not_email as $key => $mail) 
                {
                    $notificacion = new notificacion();
                    $notificacion->not_email = $mail;
                    $notificacion->usu_id = $usuario->id;
                    $notificacion->save();
                }
                
            }

            DB::commit();
            flash('Los datos del usuario se actualizaron con éxito.')->success();
            return redirect('/usuarios');

        }catch (\Exception $e) {

            DB::rollback(); 
            flash('Error al actualizar datos del usuario.')->error();
            //flash($e->getMessage())->error();
            return redirect('/usuarios');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*****

        $usuario = User::findOrfail($id);
        $pro_nombre = $usuario->name .' '.$usuario->usu_apellido;
        $usuario->delete();

        $bit = new bitacora();
        $bit->bit_transaccion = "Se ha eliminado  un proyecto con el nombre $pro_nombre del registro de la base de datos";
        $bit->bit_fecha = now();
        $bit->usu_id = Auth::user()->id;
        $bit->save();


        flash('El campo ha sido eliminado exitosamente!')->error();
        return redirect('/usuarios');

        *******/
    }

    public function desactivarUsuario()
    {
        DB::beginTransaction();
        try { 

            $usuario_id = Input::get('usuario_id');
            $usuario_estado =Input::get('usuario_estado');
            $usuario = User::where('id', $usuario_id)->first();
           
            if ($usuario_estado == 1) 
            {
                $usuario->usu_estado = 0;
            }else{
                $usuario->usu_estado = 1;
            }
            
            $usuario->update();

            DB::commit();
            return response()->json(['status' => true]);

        }catch (\Exception $e) {

            DB::rollback(); 
            //dd($e->getMessage());
            return response()->json(['status' => false]);
        }

        
    }

    public function createVisualizador($proyecto_id)
    {
        $proyecto_id =  Crypt::decrypt($proyecto_id);
        return view('usuarios.createV', compact('proyecto_id'));

        
    }

    public function storeV(CrearUsuarioRequest $request)
    {

        $validate = DB::table('users')->where('email', $request->usu_email)->exists();

        if($validate == true)
        {
            flash('El usuario '.$request->usu_email.'  ya existe en la base de datos')->warning();
            return redirect('/home');
        } else

        //verificar que el usuario posee permisos para agregar clientes visualizadores sobre este proyecto
        $permiso = DB::table('usuario_proyectos as up')
        ->where('up.pro_id', $request->proyecto_id)
        ->where('up.per_id', 1)
        ->where('up.usu_id', Auth::user()->id)
        ->exists();

        if ($permiso == false) 
        {
            flash('Usted no posee los permisos para agregar usuarios sobre este proyecto')->warning();
            return redirect('/home');
        }
        
        DB::beginTransaction();
        try {
            //guardar datos de usuario cliente visualizador
            $usuario = new User();
            $usuario->name = $request->usu_nombre;
            $usuario->usu_apellido = $request->usu_apellido;
            $usuario->password = bcrypt($request->usu_pass);
            $usuario->email = $request->usu_email;
            $usuario->emp_id = Auth::user()->oneEmpresa->id;
            $usuario->usu_tlf = $request->usu_tlf;
            $usuario->rol_id = 3;
            $usuario->usu_nombre_cargo = $request->usu_nombre_cargo;
            $usuario->usu_fecha_caducidad = null;

            $usuario->usu_estado = 1;
            $usuario->save();

            //guardar permisos asociados al proyecto del usuario recien creado
            $usuario_proyecto = new usuario_proyecto();
            $usuario_proyecto->pro_id = $request->proyecto_id;
            $usuario_proyecto->per_id = 2;
            $usuario_proyecto->usu_id = $usuario->id;
            $usuario_proyecto->save();
            
            DB::commit();

            // inicio del envio de email//
          
            $destinatariosExists = User::where('rol_id', 2)->where('id', '!=', $usuario->id)->orWhere('id', Auth::user()->id)->exists();
            if ($destinatariosExists == true) 
            {
                 $destinatarios = User::where('rol_id', 2)->where('id', '!=', $usuario->id)->orWhere('id', Auth::user()->id)->pluck('email');
            }

            if ($destinatariosExists == true) 
            {
                $subject = 'Usuario creado exitosamente';
                $rol = rol::where('id', 3)->first(['rol_nombre']);
                $texto = 'Usuario  visualizador <br> '.$request->usu_email.' <br> creado con éxito';
                $nombreCompleto = $request->usu_nombre.' '.$request->usu_apellido; 

                $body = tranformBody($texto, $nombreCompleto, $request->usu_email, $rol->rol_nombre);

                helperSendMail($subject, $body, $destinatarios, $request->usu_email);
            }else{
                flash('Fallo al enviar el email.')->error();
            }
            
            // inicio del envio de email//
            
            flash('El usuario visualizador ha sido creado exitosamente!')->success();
            return redirect('/home');

        }catch (\Exception $e) {

            DB::rollback();
            flash('Fallo al crear el usuario.')->error();
            //flash($e->getMessage())->error();
            return redirect('/home');
        }
    }


    public function asignarVisualizador($pro_id)
    {
        $pro_id = Crypt::decrypt($pro_id);

        $visualizadores = DB::table('users as u')
        ->where('u.emp_id', Auth::user()->emp_id)
        ->where('u.id', '!=', Auth::user()->id)
        ->select(DB::raw("u.id, CONCAT(u.name, ' ', u.usu_apellido) as nombreApellido"))
        ->pluck('nombreApellido','u.id');

        $proyectos = DB::table('proyectos as p')
        ->join('usuario_proyectos as up', 'p.id', 'up.pro_id')
        ->where('up.usu_id', Auth::user()->id)
        ->where('up.per_id', 1)
        ->pluck('p.pro_nombre','p.id');
        
        $proyecto = proyecto::where('id', $pro_id)->first();

        $usuariosPermiso = usuario_proyecto::where('pro_id', $pro_id)
        ->where('per_id', 2)
        ->pluck('usu_id');
        
        return view('usuarios.asignarV', compact('visualizadores', 'proyectos', 'proyecto', 'usuariosPermiso'));

        
    }

    public function asignarV(Request $request)
    {   
        DB::beginTransaction();
        try {
            //elimina los usuarios desmarcados
            $usuariosDesmarcado = usuario_proyecto::where('pro_id', $request->proyecto_id)
            ->where('per_id', 2)
            ->whereNotIn('usu_id', $request->usu_id)
            ->delete();
            
            if ($request->usu_id != null) 
            {
                foreach ($request->usu_id as $key => $usu_id) 
                {
                    $usuariosExiste = usuario_proyecto::where('pro_id', $request->proyecto_id)
                    ->where('per_id', 2)
                    ->where('usu_id', $usu_id)
                    ->exists();

                    if ($usuariosExiste == false) 
                    {
                        $newUsuarioPermiso = new usuario_proyecto();
                        $newUsuarioPermiso->pro_id = $request->proyecto_id;
                        $newUsuarioPermiso->per_id = 2;
                        $newUsuarioPermiso->usu_id = $usu_id;
                        $newUsuarioPermiso->save();
                    }
                    
                }
            }
            
            DB::commit();
            flash('Permisos actualizados exitosamente.')->success();
            return redirect()->back();            
        }catch (\Exception $e) {

            DB::rollback();
            flash('Fallo al Actualizar los permisos para los usuarios.')->error();
            //flash($e->getMessage())->error();
            return redirect()->back();
        }
    }

    public function asignarUrl($pro_id)
    {
        $pro_id = Crypt::decrypt($pro_id);

        $url = DB::table('url_proyectos as urp')
        ->where('pro_id', $pro_id)
        ->pluck('urp.url_nombre', 'urp.id');

        $usuariosUrl = usuario_proyecto::where('pro_id', $pro_id)
        ->where('per_id', 2)
        ->whereNotNull('url_id')
        ->get(['url_id', 'usu_id'])->toArray();

        $usuariosUrlArray = array();
        $usuariosUrlArray2 = array();

        foreach ($usuariosUrl as $key => $value) 
        {
            //$usuariosUrlArray[] = array();
            
            if (array_key_exists($value['usu_id'],$usuariosUrlArray2)) 
            {
                $usuariosUrlArray2[$value['usu_id']][] = $value['url_id'];
            }else{
                $usuariosUrlArray2[$value['usu_id']] = array_add($usuariosUrlArray, $key, $value['url_id']);
            }
            
        }
        
        $visualizadores = DB::table('users as u')
        ->join('usuario_proyectos as up','u.id','up.usu_id')
        ->where('up.per_id', 2)
        ->where('pro_id', $pro_id)
        ->where('u.id', '!=', Auth::user()->id)
        ->select(DB::raw("u.id, CONCAT(u.name, ' ', u.usu_apellido) as nombreApellido"))
        ->pluck('nombreApellido','u.id');

        $proyectos = DB::table('proyectos as p')
        ->join('usuario_proyectos as up', 'p.id', 'up.pro_id')
        ->where('up.usu_id', Auth::user()->id)
        ->where('up.per_id', 1)
        ->pluck('p.pro_nombre','p.id');
        
        $proyecto = proyecto::where('id', $pro_id)->first();

        $usuariosPermiso = usuario_proyecto::where('pro_id', $pro_id)
        ->where('per_id', 2)
        ->pluck('usu_id');
        
        return view('usuarios.asignarU', compact('visualizadores', 'proyectos', 'proyecto', 'usuariosPermiso', 'url', 'usuariosUrlArray2'));
    }

    public function asignarU(Request $request)
    {  

        DB::beginTransaction();
        try {      
            if ($request->visualizador != null) 
            {

                foreach ($request->visualizador as $key1 => $visualizador) 
                {
                    
                    if ($request->usu_id != null) 
                    {
                        $usuariosUrl = usuario_proyecto::where('pro_id', $request->proyecto_id)
                        ->where('per_id', 2)
                        ->where('usu_id', $visualizador)
                        ->whereNotNull('url_id')
                        ->delete();

                        foreach ($request->usu_id as $key2 => $urls) 
                        {
                            if ($key2 == $visualizador) 
                            {
                                

                                foreach ($urls as $key3 => $url) 
                                {
                                    $newVisualizadorPermiso = new usuario_proyecto();
                                    $newVisualizadorPermiso->pro_id = $request->proyecto_id;
                                    $newVisualizadorPermiso->per_id = 2;
                                    $newVisualizadorPermiso->usu_id = $key2;
                                    $newVisualizadorPermiso->url_id = $url;
                                    $newVisualizadorPermiso->save(); 
                                }
                                
                            }
                            
                           /* $usuariosUrl = usuario_proyecto::where('pro_id', $request->proyecto_id)
                            ->where('per_id', 2)
                            ->where('usu_id', $visualizador)
                            ->whereNotIn('url_id', $urls)
                            ->delete();*/
                        }
                        
/*
                        foreach ($request->usu_id   as $key => $url) 
                        {
                            $newVisualizadorPermiso = new usuario_proyecto();
                            $newVisualizadorPermiso->pro_id = $request->proyecto_id;
                            $newVisualizadorPermiso->per_id = 2;
                            $newVisualizadorPermiso->usu_id = $visualizador;
                            $newVisualizadorPermiso->url_id = $url;
                            $newVisualizadorPermiso->save();
                        }
                        */
                    }
                    else{
                        //si es null entonces se le quitaron todas las urls a todos los usuarios
                        $usuariosDesmarcados = usuario_proyecto::where('pro_id', $request->proyecto_id)
                        ->where('per_id', 2)
                        ->where('usu_id', $visualizador)
                        ->whereNotNull('url_id')
                        ->delete();
                    }
                }
                
            }
            DB::commit();
            flash('URL actualizadas exitosamente.')->success();
            return redirect()->back();            
        }catch (\Exception $e) {

            DB::rollback();
            flash('Fallo al Actualizar URL.')->error();
            //flash($e->getMessage())->error();
            return redirect()->back();
        }
        
    }
}
