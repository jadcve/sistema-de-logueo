<!-- Sidebar -->
<!-- <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar"> -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="icono-imagen sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo e(route('home')); ?>">
        <div class="sidebar-brand-icon">
           <img class="img-fluid" src="<?php echo e(asset('img/KPI-Estudios-blanco.png')); ?>" alt="Logo KPI Estudios">
        </div>
    </a>
    

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('home')); ?>">
            <i class="fas fa-home"></i>
            <span>Inicio</span></a>
    </li>
     <?php if(Auth::user()->oneRol->rol_nombre == 'SuperAdministrador'): ?>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('empresa.index')); ?>">
            <i class="fas fa-building"></i>
                <span>Empresas</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('usuarios.index')); ?>">
            <i class="fas fa-users"></i>
                <span>Usuarios</span></a>
        </li>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('proyectos.index')); ?>">
                    <i class="fas fa-chart-pie"></i>
                    <span>Proyectos</span>
                </a>

            </li>



        <!-- Nav Item - Utilities Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#configuraciones" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-cog"></i>
                <span>Configuración</span>
            </a>
            <div id="configuraciones" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-menu py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo e(route('pais.index')); ?>">Países</a>
                <a class="collapse-item" href="<?php echo e(route('rubro.index')); ?>">Rubro</a>
                <a class="collapse-item" href="<?php echo e(route('subrubro.index')); ?>">Sub Rubro</a>
                <a class="collapse-item" href="<?php echo e(route('bitacora.index')); ?>">Bitacora</a>

                </div>
            </div>
        </li>

        <!-- Nav Item - Utilities Collapse Menu -->

     <?php endif; ?>

     <?php if(Auth::user()->oneRol->rol_nombre == 'Administrador'): ?>

     <?php endif; ?>

     <?php if(Auth::user()->oneRol->rol_nombre == 'Cliente'): ?>
        <?php $__currentLoopData = Session::get('proyectos'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#proyecto-<?php echo e($key); ?>" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="far fa-folder-open"></i>
                    <span><?php echo e($p->p_pro_nombre); ?></span>
                </a>
                <div id="proyecto-<?php echo e($key); ?>" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-menu py-2 collapse-inner rounded">
                    <?php if($p->pm_per_nombre == 'Visualizador'): ?>
                        <a class="collapse-item" href="#">Ver Reporteria</a>
                    <?php else: ?>
                        <a class="collapse-item" href="<?php echo e(route('data.create',  Crypt::encrypt($p->p_id))); ?>">Cargar Datos</a>
                        <a class="collapse-item" href="<?php echo e(route('data.show', Crypt::encrypt($p->p_id))); ?>">Carga Historica</a>
                        <a class="collapse-item" href="<?php echo e(route('usuarios.createV', Crypt::encrypt($p->p_id))); ?>">Agregar Usuarios</a>
                        <a class="collapse-item" href="<?php echo e(route('usuarios.asignarVisualizador', Crypt::encrypt($p->p_id))); ?>">Asignar Usuarios</a>
                        <a class="collapse-item" href="<?php echo e(route('proyectos.lista', Crypt::encrypt($p->p_id))); ?>">Ver Reporteria</a>
                    <?php endif; ?>
                    

                    </div>
                </div>
            </li>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     <?php endif; ?>
    
    
    
   
    <!-- Nav Item - Charts 
    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
    </li>
-->
    <!-- Nav Item - Tables 
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li>
-->
    <!-- Divider
    <hr class="sidebar-divider d-none d-md-block">
    -->
</ul>
<!-- End of Sidebar -->