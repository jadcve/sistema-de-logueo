<?php

use Illuminate\Database\Seeder;
use App\rol;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	'rol_nombre' => 'SuperAdministrador',
    	]);

    	DB::table('roles')->insert([
        	'rol_nombre' => 'Administrador',
    	]);

        DB::table('roles')->insert([
            'rol_nombre' => 'Cliente',
        ]);

    }
}
