
<?php $__env->startSection('title','Sub Rubro Crear'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Crear Sub rubro</h5>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-6 offset-sm-3 mt-4">
                <?php echo Form::open(['route'=> 'subrubro.store', 'method'=>'POST']); ?>

                <div class="form-group">
                    <?php echo Form::label('Nombre del Sub Rubro'); ?>

                    <?php echo Form::text('sub_nombre', null, ['placeholder'=>'Sub rubro', 'class'=>'form-control', 'required']); ?>

                </div>
                <div class="form-group">
                    <?php echo Form::label('Seleccione el rubro'); ?>

                    <?php echo Form::select('rub_id', $rubro, null,['class'=>'form-control', 'required']); ?>

                </div>
                <div class="text-center">
                   <?php echo Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                   <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>