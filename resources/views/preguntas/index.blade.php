@extends('layouts.app2')
@section('title','Cuestionario index')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h4>Listado de Usuarios</h4>
            </div>
            <hr class="mb-4">


            <div class="table-responsive">
                <table class="table table-hover" id="dataTableAusentismo" width="100%" cellspacing="0">
                    <thead>
	                    <tr>
	                        <th>Nombres</th>
                            <th>Correo</th>
	                        <th>Rol</th>
                             @if(Auth::user()->oneRol->rol_nombre == 'Evaluador')
	                        <th>Aplicar Evaluaci&oacute;n</th>
                            @endif
                            <th>Ver Resultados</th>
	                    </tr>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $us)
                        
                        <tr>
                            <td><small>{{ $us->name}}</small></td>
                            <td><small>{{ $us->email }}</small></td>
                            <td><small>{{ $us->oneRol->rol_nombre }}</small></td>
                            @if(Auth::user()->oneRol->rol_nombre == 'Evaluador')
                            <td>
                                <small>
                                    <a href="{{ route('preguntas.create', Crypt::encrypt($us->id)) }}" class="btn-empresa"><i class="far fa-edit"></i></a>
                                </small>
                            </td>
                            @endif

                            <td>
                                <small>
                                    <a href="{{ route('preguntas.show', Crypt::encrypt($us->id)) }}" class="btn-empresa"><i class="far fa-eye"></i></a>
                                </small>

                            </td>

                        </tr>
                        
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            </div>
        </div>
    </div>
@stop