<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('usu_rut',100);
            $table->string('usu_cargo', 100)->nullable();
            $table->boolean('usu_estado');
            $table->dateTime('usu_fecha_caducidad')->nullable();;
            $table->string('usu_perfil','100')->nullable();
            $table->unsignedInteger('rol_id');
            $table->foreign('rol_id')->references('id')->on('roles')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('usu_rut');
            $table->dropColumn('usu_cargo');
            $table->dropColumn('usu_estado');
            $table->dropColumn('usu_fecha_caducidad');
            $table->dropColumn('usu_perfil');
            $table->dropForeign('users_rol_id_foreign');
            $table->dropColumn('rol_id');

        });
    }
}
