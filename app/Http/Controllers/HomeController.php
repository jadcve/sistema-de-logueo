<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\PreventBackHistory;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Http\Middleware\CheckSession;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(PreventBackHistory::class);
        $this->middleware(CheckSession::class);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

            flash('Bienvenido a la plataforma');
            return view('home');
    }

    public function index_kpi(Request $request)
    {
        
        return view('carga_kpi');
    }

    public function carga_kpi(Request $request)
    {
        return view('carga_kpi');
    }
}
