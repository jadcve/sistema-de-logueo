<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (!function_exists('helperSendMail')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function tranformBodyInicioSesion($texto, $nombreCompleto, $email, $rol)
    {
      $body_usuario_creado = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
<head>
  <!--[if gte mso 9]>s
  <xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
  </xml>
  <![endif]-->
  <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
  <meta name=\"format-detection\" content=\"date=no\" />
  <meta name=\"format-detection\" content=\"address=no\" />
  <meta name=\"format-detection\" content=\"telephone=no\" />
  <meta name=\"x-apple-disable-message-reformatting\" />
    <!--[if !mso]><!-->
  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.5.0/css/all.css\" integrity=\"sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU\" crossorigin=\"anonymous\">
    <!--<![endif]-->
  <title>Mensaje Recibido</title>
  <!--[if gte mso 9]>
  <style type=\"text/css\" media=\"all\">
    sup { font-size: 100% !important; }
  </style>
  <![endif]-->
  

  <style type=\"text/css\" media=\"screen\">
    /* Linked Styles */
    body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none }
    a { color:#66c7ff; text-decoration:none }
    p { padding:0 !important; margin:0 !important } 
    img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
    .mcnPreviewText { display: none !important; }

    .cke_editable,
    .cke_editable a,
    .cke_editable span,
    .cke_editable a span { color: #000001 !important; }   
    /* Mobile styles */
    @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
      .mobile-shell { width: 100% !important; min-width: 100% !important; }
      .bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }
      
      .text-header,
      .m-center { text-align: center !important; }
      
      .center { margin: 0 auto !important; }
      .container { padding: 0px 10px 10px 10px !important }
      
      .td { width: 100% !important; min-width: 100% !important; }

      .text-nav { line-height: 28px !important; }
      .p30 { padding: 15px !important; }

      .m-br-15 { height: 15px !important; }
      .p30-15 { padding: 30px 15px !important; }
      .p40 { padding: 20px !important; }

      .m-td,
      .m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

      .m-block { display: block !important; }

      .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

      .column,
      .column-top,
      .column-empty,
      .column-empty2,
      .column-dir-top { float: left !important; width: 100% !important; display: block !important; }
      .column-empty { padding-bottom: 10px !important; }
      .column-empty2 { padding-bottom: 20px !important; }
      .content-spacing { width: 15px !important; }
    }
  </style>
</head>
<body class=\"body\" style=\"padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none;\">

  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f4f4f4\">
    <tr>
      <td align=\"center\" valign=\"top\">
        <table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-shell\">
          <tr>
            <td class=\"td container\" style=\"width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:0px 0px 40px 0px;\">
              <!-- Header -->
              
              <!-- Hero -->
              <div mc:repeatable=\"Select\" mc:variant=\"Hero\" style=\"\" bgcolor=\"#ffff\">
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                  <tr>
                    <td class=\"fluid-img\" style=\"background: #fff; font-size:0pt; line-height:0pt; text-align:left;\"><img src=\"http://www.kpiestudios.com/bi2019/images/header-confirmacion.jpg\" width=\"100%\" height=\"160\" mc:edit=\"image_2\" style=\"max-width:100%;\" border=\"0\" alt=\"\" />
                    </td>
                    
                  </tr>
                </table>
              </div>
            <!-- END Hero -->
              <!-- END Header -->

              <!-- datos de inscripcion -->
              <div mc:repeatable=\"Select\" mc:variant=\"Nav\">
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                  <tr>
                    <td class=\"p30-15\" style=\"padding: 25px 30px;\" bgcolor=\"#fff\" align=\"center\">
                      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                          <td class=\"text-nav\" style=\"color:#2462a7; font-family: Arial,sans-serif; font-size:28px; line-height:28px; text-align:center;\">
                            <div mc:edit=\"text_2\">
                              <span class=\"link-white\" style=\"color:#2462a7; text-decoration:none;\">$texto</span>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class=\"p30-15\" style=\"padding: 30px 60px 10px;\" bgcolor=\"#fff\" align=\"center\">
                      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                          <td class=\"text-nav\" style=\"color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:24px; line-height:18px; text-align:center; \">
                            <div mc:edit=\"text_2\">
                              <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">Datos del usuario</span>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  
                  
                  
                </table>
              </div>
              <!-- END Datos de inscripcion -->

<!-- DATOS DE PARTICIPANTE -->
<div mc:repeatable=\"Select\">
  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
   <td bgcolor=\"#ffffff\" style=\"padding:40px 30px 80px 30px;\">
    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
      <tbody>
      <tr>
        <td style=\"padding:0px 30px 20px 0px; color:#153643;font-family:Arial,sans-serif;font-size:14px \">
        <!--<b>Participante Nº 54</b>-->
        </td>
      </tr>

      <tr>
        <td>
        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
          <tbody>
           <tr>
             <td width=\"260\" valign=\"top\">
              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
              <tbody>
                 <tr>
                   <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:14px; line-height:18px; text-align: left; \">
                   <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none; \">Nombre:</span>
                   </div>
                  </td>
                 </tr>
                 <tr>
                     <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:14px; line-height:18px; text-align: left; \">
                   <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">Email :</span>
                   </div>
                  </td>
                 </tr>
                 <tr>
                     <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:14px; line-height:18px; text-align: left; \">
                   <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">Rol:</span>
                   </div>
                  </td>
                 </tr>
                
               </tbody>
              </table>
             </td>
             <td style=\"font-size:0;line-height:0\" width=\"20\">
                          &nbsp;
             </td>
             <td width=\"260\" valign=\"top\">
              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
              <tbody>
                <tr>
                 <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align: left; \">
                  <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\"> $nombreCompleto</span>
                  </div>
                 </td>
                </tr> 
                <tr>
                    <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align: left; \">
                  <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">$email</span>
                  </div>
                 </td>
                </tr>
                <tr>
                 <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align: left; \">
                  <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">$rol</span>
                  </div>
                 </td>
                </tr>
                
              </tbody>
              </table>
             </td>
           </tr>
          </tbody>
        </table>
        </td>
      </tr>
      </tbody>
    </table>
   </td>
  </table>
</div>
<!-- END DATOS DE PARTICIPANTE -->
              
              
            
<!-- Three Columns -->
 <div mc:repeatable=\"Select\" mc:variant=\"Three Columns\">
  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tbody><tr>
    <td class=\"pb10\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
    <!-- Columns -->
      <tbody><tr mc:repeatable=\"\">
      <td>
        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tbody><tr>
           <th class=\"column-empty\" bgcolor=\"#2462a7\" width=\"10\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; \"> 
           </th>
           <th class=\"column-top\" width=\"50%\" bgcolor=\"#2462a7\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; \">
          <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
             <tbody><tr>
             <td class=\"p30\" style=\"padding:30px;\">
              <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                 <tbody><tr>
                  <td class=\"h3 pb15\" style=\"color:#fff; font-family: Arial,sans-serif; font-size:14px;text-align:left;\"><div mc:edit=\"text_17\">Visítanos</div></td>
                 </tr>
                 <tr>
                 <td class=\"text pb25\" style=\"padding-top:20px; color:#fff; font-family:'Roboto', Arial,sans-serif; font-size:16px; line-height:28px; text-align:left;\">
                  <div mc:edit=\"text_18\"><a class=\"link-white\" style=\"color:#ffffff; text-decoration:none;\"http://www.kpiestudios.com\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none;\">http://www.kpiestudios.com</span></a></div>
                 </td>
                 </tr>                        
              </tbody></table>
             </td>
             </tr>
          </tbody></table>
           </th>
                          
           <th class=\"column-empty\" bgcolor=\"#2462a7\" width=\"10\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">
           </th>
                          
           <th class=\"column-top\" width=\"210\" bgcolor=\"#2462a7\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">
               <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                <tbody><tr>
                <td class=\"p30\" style=\"padding:30px;\">
                   <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                    <tbody><tr>
                      <td class=\"h3 pb15\" style=\"color:#fff; font-family:Arial,sans-serif; font-size:14px;text-align:left; \"><div mc:edit=\"text_20\">Síguenos</div></td>
                    </tr>
                    <tr>
                    <td class=\"text pb25\" style=\"padding-top:20px; color:#fff; font-family: Arial,sans-serif; font-size:16px; line-height:28px; text-align:left;\">
                            
                     <table><!--icono redes-->
                       <tbody>
                      <tr>
                        <td>
                         <a class=\"link-white\" style=\"color:#ffffff; text-decoration:none;\" href=\"https://www.linkedin.com/company/kpi-estudios/\"><span class=\"link-white\">
                           <img src=\"http://www.kpiestudios.com/bi2019/images/linkedin.png\" alt=\"\"></span></a>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        
                        <td>&nbsp;</td>
                       </tr>
                       </tbody>
                                         </table><!--/icono redes-->
                   
                              
                      </td>
                    </tr>                     
                   </tbody></table>
                </td>
                </tr>
               </tbody></table>
            </th>
           </tr>
        </tbody></table>
      </td>
      </tr>
    <!-- END Columns -->                  
    </tbody></table>
    </td>
  </tr>
  </tbody></table>
 </div>
<!-- END Three Columns -->
               
<!-- Footer -->
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
   <tr>
    <td class=\"p30-15\" style=\"padding: 5px 30px;\" bgcolor=\"#2462a7\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td class=\"text-footer2 pb30\" style=\"color:#fff; font-family:Arial,sans-serif; font-size:12px; line-height:26px; text-align:center; padding-bottom:30px;\"><div mc:edit=\"text_36\">@ KPI Estudios 2019, todos los derechos reservados</div></td>
      </tr>
    </table>
    </td>
   </tr>
</table>
<!-- END Footer -->
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</body>
</html>";
  return $body_usuario_creado;
    }

    function tranformBody($texto, $nombreCompleto, $email, $rol)
    {
      $body_usuario_creado = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
<head>
  <!--[if gte mso 9]>s
  <xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
  </xml>
  <![endif]-->
  <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
  <meta name=\"format-detection\" content=\"date=no\" />
  <meta name=\"format-detection\" content=\"address=no\" />
  <meta name=\"format-detection\" content=\"telephone=no\" />
  <meta name=\"x-apple-disable-message-reformatting\" />
    <!--[if !mso]><!-->
  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\" />
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.5.0/css/all.css\" integrity=\"sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU\" crossorigin=\"anonymous\">
    <!--<![endif]-->
  <title>Mensaje Recibido</title>
  <!--[if gte mso 9]>
  <style type=\"text/css\" media=\"all\">
    sup { font-size: 100% !important; }
  </style>
  <![endif]-->
  

  <style type=\"text/css\" media=\"screen\">
    /* Linked Styles */
    body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none }
    a { color:#66c7ff; text-decoration:none }
    p { padding:0 !important; margin:0 !important } 
    img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
    .mcnPreviewText { display: none !important; }

    .cke_editable,
    .cke_editable a,
    .cke_editable span,
    .cke_editable a span { color: #000001 !important; }   
    /* Mobile styles */
    @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
      .mobile-shell { width: 100% !important; min-width: 100% !important; }
      .bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }
      
      .text-header,
      .m-center { text-align: center !important; }
      
      .center { margin: 0 auto !important; }
      .container { padding: 0px 10px 10px 10px !important }
      
      .td { width: 100% !important; min-width: 100% !important; }

      .text-nav { line-height: 28px !important; }
      .p30 { padding: 15px !important; }

      .m-br-15 { height: 15px !important; }
      .p30-15 { padding: 30px 15px !important; }
      .p40 { padding: 20px !important; }

      .m-td,
      .m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

      .m-block { display: block !important; }

      .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

      .column,
      .column-top,
      .column-empty,
      .column-empty2,
      .column-dir-top { float: left !important; width: 100% !important; display: block !important; }
      .column-empty { padding-bottom: 10px !important; }
      .column-empty2 { padding-bottom: 20px !important; }
      .content-spacing { width: 15px !important; }
    }
  </style>
</head>
<body class=\"body\" style=\"padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none;\">

  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f4f4f4\">
    <tr>
      <td align=\"center\" valign=\"top\">
        <table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-shell\">
          <tr>
            <td class=\"td container\" style=\"width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:0px 0px 40px 0px;\">
              <!-- Header -->
              
              <!-- Hero -->
              <div mc:repeatable=\"Select\" mc:variant=\"Hero\" style=\"\" bgcolor=\"#ffff\">
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                  <tr>
                    <td class=\"fluid-img\" style=\"background: #fff; font-size:0pt; line-height:0pt; text-align:left;\"><img src=\"http://www.kpiestudios.com/bi2019/images/header-confirmacion.jpg\" width=\"100%\" height=\"160\" mc:edit=\"image_2\" style=\"max-width:100%;\" border=\"0\" alt=\"\" />
                    </td>
                    
                  </tr>
                </table>
              </div>
            <!-- END Hero -->
              <!-- END Header -->

              <!-- datos de inscripcion -->
              <div mc:repeatable=\"Select\" mc:variant=\"Nav\">
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                  <tr>
                    <td class=\"p30-15\" style=\"padding: 25px 30px;\" bgcolor=\"#fff\" align=\"center\">
                      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                          <td class=\"text-nav\" style=\"color:#2462a7; font-family: Arial,sans-serif; font-size:28px; line-height:28px; text-align:center;\">
                            <div mc:edit=\"text_2\">
                              <span class=\"link-white\" style=\"color:#2462a7; text-decoration:none;\">$texto</span>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class=\"p30-15\" style=\"padding: 30px 60px 10px;\" bgcolor=\"#fff\" align=\"center\">
                      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                          <td class=\"text-nav\" style=\"color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:24px; line-height:18px; text-align:center; \">
                            <div mc:edit=\"text_2\">
                              <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">Datos del usuario</span>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  
                  
                  
                </table>
              </div>
              <!-- END Datos de inscripcion -->

<!-- DATOS DE PARTICIPANTE -->
<div mc:repeatable=\"Select\">
  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
   <td bgcolor=\"#ffffff\" style=\"padding:40px 30px 80px 30px;\">
    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
      <tbody>
      <tr>
        <td style=\"padding:0px 30px 20px 0px; color:#153643;font-family:Arial,sans-serif;font-size:14px \">
        <!--<b>Participante Nº 54</b>-->
        </td>
      </tr>

      <tr>
        <td>
        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
          <tbody>
           <tr>
             <td width=\"260\" valign=\"top\">
              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
              <tbody>
                 <tr>
                   <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:14px; line-height:18px; text-align: left; \">
                   <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none; \">Nombre:</span>
                   </div>
                  </td>
                 </tr>
                 <tr>
                     <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:14px; line-height:18px; text-align: left; \">
                   <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">Email :</span>
                   </div>
                  </td>
                 </tr>
                 <tr>
                     <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-weight: bold; font-size:14px; line-height:18px; text-align: left; \">
                   <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">Rol:</span>
                   </div>
                  </td>
                 </tr>
                
               </tbody>
              </table>
             </td>
             <td style=\"font-size:0;line-height:0\" width=\"20\">
                          &nbsp;
             </td>
             <td width=\"260\" valign=\"top\">
              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
              <tbody>
                <tr>
                 <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align: left; \">
                  <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\"> $nombreCompleto</span>
                  </div>
                 </td>
                </tr> 
                <tr>
                    <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align: left; \">
                  <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">$email</span>
                  </div>
                 </td>
                </tr>
                <tr>
                 <td class=\"text-nav\" style=\"padding-bottom: 5px; color:#ffffff; font-family:Arial,sans-serif; font-size:14px; line-height:18px; text-align: left; \">
                  <div mc:edit=\"text_2\">
                    <span class=\"link-white\" style=\"color:#565d7b; text-decoration:none;\">$rol</span>
                  </div>
                 </td>
                </tr>
                
              </tbody>
              </table>
             </td>
           </tr>
          </tbody>
        </table>
        </td>
      </tr>
      </tbody>
    </table>
   </td>
  </table>
</div>
<!-- END DATOS DE PARTICIPANTE -->
              
              
            
<!-- Three Columns -->
 <div mc:repeatable=\"Select\" mc:variant=\"Three Columns\">
  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tbody><tr>
    <td class=\"pb10\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
    <!-- Columns -->
      <tbody><tr mc:repeatable=\"\">
      <td>
        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tbody><tr>
           <th class=\"column-empty\" bgcolor=\"#2462a7\" width=\"10\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; \"> 
           </th>
           <th class=\"column-top\" width=\"50%\" bgcolor=\"#2462a7\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; \">
          <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
             <tbody><tr>
             <td class=\"p30\" style=\"padding:30px;\">
              <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                 <tbody><tr>
                  <td class=\"h3 pb15\" style=\"color:#fff; font-family: Arial,sans-serif; font-size:14px;text-align:left;\"><div mc:edit=\"text_17\">Visítanos</div></td>
                 </tr>
                 <tr>
                 <td class=\"text pb25\" style=\"padding-top:20px; color:#fff; font-family:'Roboto', Arial,sans-serif; font-size:16px; line-height:28px; text-align:left;\">
                  <div mc:edit=\"text_18\"><a class=\"link-white\" style=\"color:#ffffff; text-decoration:none;\" href=\"https://www.kpiestudios.com\" target=\"_blank\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none;\">www.kpiestudios.com</span></a></div>
                 </td>
                 </tr>                        
              </tbody></table>
             </td>
             </tr>
          </tbody></table>
           </th>
                          
           <th class=\"column-empty\" bgcolor=\"#2462a7\" width=\"10\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">
           </th>
                          
           <th class=\"column-top\" width=\"210\" bgcolor=\"#2462a7\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">
               <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                <tbody><tr>
                <td class=\"p30\" style=\"padding:30px;\">
                   <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                    <tbody><tr>
                      <td class=\"h3 pb15\" style=\"color:#fff; font-family:Arial,sans-serif; font-size:14px;text-align:left; \"><div mc:edit=\"text_20\">Síguenos</div></td>
                    </tr>
                    <tr>
                    <td class=\"text pb25\" style=\"padding-top:20px; color:#fff; font-family: Arial,sans-serif; font-size:16px; line-height:28px; text-align:left;\">
                            
                     <table><!--icono redes-->
                       <tbody>
                      <tr>
                        <td>
                         <a class=\"link-white\" style=\"color:#ffffff; text-decoration:none;\" href=\"https://www.linkedin.com/company/kpi-estudios/\" target=\"_blank\"><span class=\"link-white\">
                           <img src=\"http://www.kpiestudios.com/bi2019/images/linkedin.png\" alt=\"\"></span></a>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        
                        <td>&nbsp;</td>
                       </tr>
                       </tbody>
                    </table><!--/icono redes-->
                   
                              
                      </td>
                    </tr>                     
                   </tbody></table>
                </td>
                </tr>
               </tbody></table>
            </th>
           </tr>
        </tbody></table>
      </td>
      </tr>
    <!-- END Columns -->                  
    </tbody></table>
    </td>
  </tr>
  </tbody></table>
 </div>
<!-- END Three Columns -->
               
<!-- Footer -->
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
   <tr>
    <td class=\"p30-15\" style=\"padding: 5px 30px;\" bgcolor=\"#2462a7\">
    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td class=\"text-footer2 pb30\" style=\"color:#fff; font-family:Arial,sans-serif; font-size:12px; line-height:26px; text-align:center; padding-bottom:30px;\"><div mc:edit=\"text_36\">@ KPI Estudios 2019, todos los derechos reservados</div></td>
      </tr>
    </table>
    </td>
   </tr>
</table>
<!-- END Footer -->
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</body>
</html>";
  return $body_usuario_creado;
    }
  function helperSendMail( $sujeto, $body , $destinatarios, $usuarioCreado = '', $archivo = '')
    {

    	try {
        $mail = new PHPMailer(true);
    
        //Server settings
        //$mail->SMTPDebug = 3;                                 // Enable verbose debug output
     
        $mail->isSMTP(); 
        
        $mail->Mailer = trim('smtp');
        $mail->Host = trim('smtp.gmail.com');
        $mail->SMTPAuth = true;
        $mail->Username   = 'envio@kpiestudios.com';                     // SMTP username
        $mail->Password   = 'oyilvyfekypcwxxn'; 
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        
        $mail->From = trim('comunitascl@gmail.com');
        $mail->FromName = trim('Alain Diaz');
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;  

        if (is_file($archivo)) {
          $mail->addAttachment($archivo);  
          
        }
        
               // Add attachments
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');  

        $mail->Subject = $sujeto;
        $mail->Body = html_entity_decode($body);
        $mail->AltBody = trim('Mail de Notificacion');
        $mail->IsHTML(true);
        
        foreach ($destinatarios as $key => $value) 
        {
          $mail->addBCC($value);
        }
        
        if ($usuarioCreado != '') 
        {
            $mail->AddCC($usuarioCreado);
        }  
        
          
    

        //$mail->AddBCC("ga@kpiestudios.com");
        $mail->send();
        //echo 'Message has been sent';
        
    } catch (Exception $e) {
        //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        
    }

    
  }
}
