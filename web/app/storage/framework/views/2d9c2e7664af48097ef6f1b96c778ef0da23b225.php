
<?php $__env->startSection('title','Pais Index'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Listado de paises</h5>
            </div>
            <hr class="mb-4">
            <div class="col-lg-12 pb-3 pt-2">
                <a href="<?php echo e(route('pais.create')); ?>" class = 'btn btn-primary'>Crear nuevo Pais</a>
            </div>
            
            <div class="ibox-content">
                <table class="table table-hover" id="dataTableAusentismo" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Paises</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $__currentLoopData = $pais; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                            <td><?php echo e($p->pai_nombre); ?></td>
                            <td>
                                <small>
                                    <a href="<?php echo e(route('pais.edit', Crypt::encrypt($p->id))); ?>" class="btn-empresa"><i class="far fa-edit"></i></a>
                                </small>
<!--
                                <a href = "<?php echo e(route('pais.destroy', $p->id)); ?>" onclick="return confirm('¿Esta seguro que desea eliminar este elemento?')" class="btn-empresa"><i class="far fa-trash-alt"></i>
-->
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                
            </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>