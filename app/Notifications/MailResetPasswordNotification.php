<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $token;

    public function __construct($token)
    {
        //
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = url( "/password/reset/" . $this->token );
        //$link = url(config('app.url').route('password.reset', $this->token, false)):
        return (new MailMessage)
                    ->subject('Restablecer contraseña.')
                    ->line('Estas recibiendo esta notificacion porque recibimos una solicitud de restablecimiento de contraseña.')
                    ->action('cambiar password', $link)
                    ->line('Este link para resetear expirara dentro de 60 minutos.
                        Si tu no hiciste esta solicitud para resetear tu contraseña, no debes hacer nada. Si crees que alguien puede estar intentando entrar a tu cuenta contactanos a contacto@dominio.com')
                    ->line('Saludos!, ')
                    ->salutation('Alain Diaz');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
