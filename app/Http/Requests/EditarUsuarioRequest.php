<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditarUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function messages()
    {
            return [
                'password.min' => 'La contraseña debe poseer un minimo de 6 caracteres',
                'password.confirmed' => 'Las contraseñas ingresadas no coinciden',
                'password.regex' => 'La contraseña contiene caracteres de al menos tres de las siguientes cinco categorías:
                    Caracteres en mayúscula  (A – Z),
                    Caracteres en  minúsculas (a – z),
                    Al menos 1 dígito (0 – 9),
                    1 o más caracteres especiales (_!&$#%)'
                   
            ];
        
        
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'password' => 'min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed|nullable'
        ];
    }
}
