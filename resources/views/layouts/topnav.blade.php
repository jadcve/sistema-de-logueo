<!-- Main Content -->
<div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>
        
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

        

            <div class="topbar-divider d-none d-sm-block"></div>

            <li class="nav-item dropdown no-arrow mt-4">
                    <span class="mr-2 d-lg-inline text-gray-600 small">{{Auth::user()->name}}</span>
                    <span class="d-none d-lg-inline">|</span>
                <span class="mr-2 d-lg-inline text-gray-600 small">{{Auth::user()->usu_cargo}}</span>
                <span class="d-none d-lg-inline">|</span>
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->oneRol->rol_nombre}}</span>
            </li>
            <!-- Nav Item - salir de sesión -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="modal" data-target="#logoutModal" id="userDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">Cerrar Sesión</span><i class="fas fa-power-off"></i>
                </a>
            </li>

        </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Breadcrumbs
        <ol class="breadcrumb">
          @yield('breadcrumb')
        </ol>-->
        @include('flash::message')
        @yield('content')
        @yield('scripts')

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->