
<?php $__env->startSection('title','Rubro Editar'); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Rubro</h5>
                </div>
                <hr class="mb-4">
                <div class="ibox-content col-lg-6 offset-sm-3 mt-4">
                    
                    <?php echo Form::open(['route'=> ['rubro.update', $rubro->id], 'method'=>'PATCH']); ?>

                        <input type = 'hidden' name = '_token' value = '<?php echo e(Session::token()); ?>'>

                    <div class="form-group">
                        <?php echo Form::label('Nombre del Rubro'); ?>

                        <?php echo Form::text('rub_nombre', $rubro->rub_nombre, ['placeholder'=>'Rubro', 'class'=>'form-control']); ?>

                    </div>
                    <div class="text-center">
                        <?php echo Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']); ?>

                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>