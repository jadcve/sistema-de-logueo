
<?php $__env->startSection('title','Cargar Datos KPI Analitics'); ?>
<?php $__env->startSection('content'); ?>

<div class="ibox float-e-margins">
    <div class="ibox-title col-sm-12">
        <h5><img width="130px" src="<?php echo e(asset('img/kpi-analytics-icono.png')); ?>" alt="Logo KPI Analytics">KPI Analytics Carga Histórica</h5>
    </div>
    <hr class="mb-4">
    <div class="row">
        <div id="tabla-historica" class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
            <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Año / Mes</th>
                        <th>Dotación</th>
                        <th>Ausentismo</th>
                        <th>Planificación</th>
                        <th>Hijos</th>
                        <th>Cursos</th>
                        <th>Asistencias</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($i = 0; $i <= 35; $i++): ?>
                     <?php $fecha = date("Y-m",strtotime($fecha_actual."- ".$i." month")) ?>
                        <tr>
                            <td><?php echo e($fecha); ?></td>
                            <td><a href="<?php echo route('dotacion.show', [Crypt::encrypt($id), $fecha]); ?>"><i class="fas <?php echo e((in_array($fecha, $fecha_dotacion_up) == true) ? 'fa-check' : 'fa-times'); ?>"></i></a></td>
                            <td><a href="<?php echo route('ausentismo.show', [Crypt::encrypt($id), $fecha]); ?>"><i class="fas <?php echo e((in_array($fecha, $fecha_ausentismo_up) == true) ? 'fa-check' : 'fa-times'); ?>"></i></a></td>
                            <td><a href="<?php echo route('planificacion.show', [Crypt::encrypt($id), $fecha]); ?>"><i class="fas <?php echo e((in_array($fecha, $fecha_planificacion_up) == true) ? 'fa-check' : 'fa-times'); ?>"></i></a></td>
                            <td><a href="<?php echo route('hijos.show', [Crypt::encrypt($id), $fecha]); ?>"><i class="fas <?php echo e((in_array($fecha, $fecha_hijos_up) == true) ? 'fa-check' : 'fa-times'); ?>"></i></a></td>
                            <td><a href="<?php echo route('cursos.show', [Crypt::encrypt($id), $fecha]); ?>"><i class="fas <?php echo e((in_array($fecha, $fecha_cursos_up) == true) ? 'fa-check' : 'fa-times'); ?>"></i></a></td>
                            <td><a href="<?php echo route('asistencia.show', [Crypt::encrypt($id), $fecha]); ?>"><i class="fas <?php echo e((in_array($fecha, $fecha_asistencia_up) == true) ? 'fa-check' : 'fa-times'); ?>"></i></a></td>
                        </tr>
                    <?php endfor; ?>
                    
                   
                </tbody>
            </table>
        </div><!-- inbox content -->
    </div>      
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>