<!-- Sidebar -->
<!-- <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar"> -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="icono-imagen sidebar-brand d-flex align-items-center justify-content-center" href="{{route('home')}}">
        <div class="sidebar-brand-icon">

        </div>
    </a>
    

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-home"></i>
            <span>Inicio</span></a>
    </li>
     @if(Auth::user()->oneRol->rol_nombre == 'SuperAdministrador')


        <!-- Nav Item - Pages Collapse Menu -->
        <!--
        <li class="nav-item">
            <a class="nav-link" href="{{route('preguntas.index')}}">
             <i class="far fa-edit"></i>
                <span>Lista de Evaluados</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
            <i class="fas fa-chart-area"></i>
                <span>Reporte</span></a>
        </li>
-->
      

        <!-- Nav Item - Utilities Collapse Menu -->

     @endif

     @if(Auth::user()->oneRol->rol_nombre == 'Administrador')

         <!-- Nav Item - Pages Collapse Menu -->
<!--      
             <li class="nav-item">
                 <a class="nav-link" href="{{route('preguntas.index')}}">
                     <i class="fas fa-chart-pie"></i>
                     <span>Ver Resultados</span></a>
             </li>


-->


         @endif

         @if(Auth::user()->oneRol->rol_nombre == 'Cliente')


        <li class="nav-item">
            <a class="nav-link" href="#">
            <i class="fas fa-chart-area"></i>
                <span>Reporte</span></a>
        </li>

      

        <!-- Nav Item - Utilities Collapse Menu -->

     @endif


    
    
    
   
    <!-- Nav Item - Charts 
    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
    </li>
-->
    <!-- Nav Item - Tables 
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li>
-->
    <!-- Divider
    <hr class="sidebar-divider d-none d-md-block">
    -->
</ul>
<!-- End of Sidebar -->