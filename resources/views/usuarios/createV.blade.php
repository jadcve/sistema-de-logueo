@extends('layouts.app2')
@section('title','Usuario Crear')
@section('content')

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Agregar usuarios</h5>
                <P>Dentro de esta sección podrás agregar usuarios visualizadores, el cual solo podrá visualizar un proyecto no editar.</P>
            </div>
            <hr class="mb-4">
            <div class="ibox-content col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-4">
                {!! Form::open(['route'=> 'usuarios.storeV', 'method'=>'POST']) !!}

                {!! Form::hidden('proyecto_id', $proyecto_id) !!}
                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre" class="col-sm-3">Nombre del usuario <strong>*</strong></label>
                        {!! Form::text('usu_nombre', null, ['placeholder'=>'Nombre del usuario', 'class'=>'form-control col-sm-9', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre" class="col-sm-3">Apellido del usuario <strong>*</strong></label>
                        {!! Form::text('usu_apellido', null, ['placeholder'=>'Apellido del usuario', 'class'=>'form-control col-sm-9', 'required']) !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <label for="usu_email" class="col-sm-3">Email <strong>*</strong></label>
                        {!! Form::text('usu_email', old('email'), ['class'=>'form-control col-sm-9', 'placeholder'=>'Email']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_pass" class="col-sm-3">Contraseña <strong>*</strong></label>
                        {{ Form::password('usu_pass',array('placeholder'=>'Contraseña','class' => 'form-control col-sm-9', 'pattern' => '^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$' ,'required')) }}

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_pass_rep" class="col-sm-3">Repita Contraseña <strong>*</strong></label>
                        {{ Form::password('usu_pass_confirmation',array('placeholder'=>'Repita la contraseña','class' => 'form-control col-sm-9', 'pattern' => '^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$' ,'required')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_tlf" class="col-sm-3">Teléfono</label>
                        {!! Form::text('usu_tlf', null, ['placeholder'=>'Telefono', 'class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label for="usu_nombre_cargo" class="col-sm-3">Cargo </label>
                        {!! Form::text('usu_nombre_cargo', null, ['placeholder'=>'Nombre del cargo', 'class'=>'form-control col-sm-9']) !!}
                    </div>
                </div>

                <div class="text-center pb-5">
                    {!! Form::submit('Registrar usuarios', ['class' => 'btn btn-primary block full-width m-b']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="text-center texto-leyenda">
                    <p><strong>*</strong> Campos obligatorios</p>
                </div>
            </div>
        </div>
@stop
